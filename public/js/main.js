$(function () {
  $("#clientErrorClose").click(function () {
    $("#clientError").fadeOut("fast");
  });

  $("#generalUpdateClose").click(function () {
    $("#generalUpdate").fadeOut("fast");
  });

  const urlParams = new URLSearchParams(location.search);
  if (urlParams.get("generalerror")) {
    showClientError(urlParams.get("title"), urlParams.get("message"));
  }

  if (urlParams.get("updated")) {
    $("#generalUpdate").fadeIn();
  }
});

function showClientError(title, message) {
  $("#clientErrorTitle").html("Something went wrong");
  $("#clientErrorMessage").html(
    "Something went wrong with that operation. Please try again."
  );
  if (title) $("#clientErrorTitle").html(title);
  if (message) $("#clientErrorMessage").html(message);
  $("#clientError").fadeIn();
}

document.addEventListener("DOMContentLoaded", () => {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(
    document.querySelectorAll(".navbar-burger"),
    0
  );

  // Add a click event on each of them
  $navbarBurgers.forEach((el) => {
    el.addEventListener("click", () => {
      // Get the target from the "data-target" attribute
      const target = el.dataset.target;
      const $target = document.getElementById(target);

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      el.classList.toggle("is-active");
      $target.classList.toggle("is-active");
    });
  });
});
