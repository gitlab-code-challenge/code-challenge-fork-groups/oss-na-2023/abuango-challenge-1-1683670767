const { updateAllProjectInfo } = require("../lib/cron/challenges");
const { checkScores } = require("../lib/cron/scoring");

const { logger } = require("../lib/logger");
const { isEnabled } = require("../lib/unleash"); 

module.exports = (router) => {
    router.get("/tasks/checkScores", async (req, res) => {
        logger.info("TASK: Checking Scores");
        await updateAllProjectInfo();
        await checkScores();
        res.send("OK");
    });

    router.get("/tasks/hourly", async (req, res) => {
        let times = [];
        for (let i = 0; i < 25; i++) {
            let time = `utc-${i}`;
            let sendIt = isEnabled(time);
            times[i] = { time, sendIt }
        }
        res.json(times);
    })
}