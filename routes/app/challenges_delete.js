const { requiresAuth } = require("express-openid-connect");

const db = require("../../db");
const { logger } = require("../../lib/logger");

module.exports = (router) => {
  router.delete(
    "/challenge/:challenge_id/completed",
    requiresAuth(),
    async (req, res) => {
      const openIDuser = await req.oidc.fetchUserInfo();
      const thisUser = await db.users.getOrCreateUser(openIDuser);
      requestUser = thisUser.id;

      const challengeId = req.params.challenge_id;
      const { userId, levelId } = req.query;
      if (userId && challengeId && levelId) {
        const userChallenge = await db.challenges.userChallenge(
          challengeId,
          requestUser
        );
        if (userChallenge.isAdmin || req.currentUser.super_admin) {
          db.scoring
            .deleteLevelCompleted(levelId, challengeId, userId)
            .then((ret) => {
              logger.debug(`Delete ${ret} records from levels_completed`);
              res.send("Ok");
            })
            .catch((e) => {
              console.error(e);
              res.status(500).json(e);
            });
        } else {
          res.status(500).send("User is not admin who tried to delete");
        }
      } else {
        logger.error(
          "User not logged in DELETE /challenge/:challenge_id/completed"
        );
        res.status(400).send("Sorry, you are not logged in");
      }
    }
  );

  router.delete(
    "/challenge/:challenge_id/entrant/:user_id",
    requiresAuth(),
    async (req, res) => {
      const openIDuser = await req.oidc.fetchUserInfo();
      const thisUser = await db.users.getOrCreateUser(openIDuser);
      requestUser = thisUser.id;

      const challengeId = req.params.challenge_id;
      const userId = req.params.user_id;
      if (userId && challengeId) {
        const userChallenge = await db.challenges.userChallenge(
          challengeId,
          requestUser
        );
        const deleteUserChallenge = await db.challenges.userChallenge(
          challengeId,
          userId
        );
        if (userChallenge.isAdmin || req.currentUser.super_admin) {
          if (deleteUserChallenge.isOwner && !req.currentUser.super_admin) {
            res.status(400).send("You cannot delete the owner sorry");
          } else {
            db.users_challenges
              .deleteEntrant(challengeId, userId)
              .then((ret) => {
                logger.debug(`Delete ${userId} from challenge ${challengeId}`);
                res.send("Ok");
              })
              .catch((e) => {
                console.error(e);
                res.status(500).json(e);
              });
          }
        } else {
          res.status(500).send("User is not admin who tried to delete");
        }
      } else {
        logger.error(
          "User not logged in DELETE /challenge/:challenge_id/completed"
        );
        res.status(400).send("Sorry, you are not logged in");
      }
    }
  );
};
