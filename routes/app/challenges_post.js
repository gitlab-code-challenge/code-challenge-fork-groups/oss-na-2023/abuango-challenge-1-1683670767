const { requiresAuth } = require("express-openid-connect");
const PubSub = require("pubsub-js");
const util = require("util");
const db = require("../../db");
const { logger } = require("../../lib/logger");

module.exports = (router) => {
  router.post("/challenge/new", requiresAuth(), async (req, res) => {
    const openIDuser = await req.oidc.fetchUserInfo();
    const thisUser = await db.users.getOrCreateUser(openIDuser);
    userId = thisUser.id;

    if (userId) {
      const newChallenge = await db.challenges.create(req.body, userId);
      const newChallengeId = newChallenge[0].id;
      res.redirect(`/app/challenge/${newChallengeId}?new=true`);
    } else {
      logger.error("User not logged in POST /challenge/new");
      res.status(400).send("Sorry, you are not logged in");
    }
  });

  router.post("/challenge/:id", requiresAuth(), async (req, res) => {
    const openIDuser = await req.oidc.fetchUserInfo();
    const thisUser = await db.users.getOrCreateUser(openIDuser);
    userId = thisUser.id;

    if (userId) {
      const userChallenge = await db.challenges.userChallenge(
        req.body.id,
        userId
      );
      console.log(util.inspect(thisUser));
      if (userChallenge.isAdmin || thisUser.super_admin) {
        logger.debug("Updating challenge in POST /challenge/:id");
        try {
          const u = await db.challenges.update(req.body, userId);
          res.redirect(`/app/challenge/${req.body.id}/settings?updated=true`);
        } catch (error) {
          logger.error(error);
          res.redirect(`/app/challenge/${req.body.id}/settings?updated=error`);
        }
      } else {
        res.redirect("/app?auth=no");
      }
    } else {
      logger.error("User not logged in POST /challenge/:id");
      res.status(400).send("Sorry, you are not logged in");
    }
  });

  router.post(
    "/challenge/:challenge_id/level/:level_id",
    requiresAuth(),
    async (req, res) => {
      const openIDuser = await req.oidc.fetchUserInfo();
      const thisUser = await db.users.getOrCreateUser(openIDuser);
      userId = thisUser.id;
      const challengeId = req.body.challengeId;

      if (userId) {
        const userChallenge = await db.challenges.userChallenge(
          challengeId,
          userId
        );
        if (userChallenge.isAdmin || thisUser.super_admin) {
          logger.debug(
            "Adding or updating level in POST /challenge/:challenge_id/level/:level_id"
          );
          try {
            if (req.params.level_id == "new") {
              db.levels.create(req.body, challengeId, userId);
            } else {
              db.levels.update(req.body, challengeId, userId);
            }
            res.redirect(`/app/challenge/${challengeId}/?updated=true`);
          } catch (error) {
            logger.error(error);
            res.redirect(`/app/challenge/${challengeId}/?updated=error`);
          }
        } else {
          res.redirect("/app?auth=no");
        }
      } else {
        logger.error(
          "User not logged in POST /challenge/:challenge_id/level/:level_id"
        );
        res.status(400).send("Sorry, you are not logged in");
      }
    }
  );

  router.post(
    "/challenge/:id/notifications",
    requiresAuth(),
    async (req, res) => {
      const openIDuser = await req.oidc.fetchUserInfo();
      const thisUser = await db.users.getOrCreateUser(openIDuser);
      userId = thisUser.id;
      const challengeId = req.body.challenge_id;

      if (userId) {
        const userChallenge = await db.challenges.userChallenge(
          challengeId,
          userId
        );
        if (userChallenge.isAdmin || thisUser.super_admin) {
          logger.debug(
            "Updating challenge weboooks in POST /challenge/:id/notifications"
          );
          try {
            db.challenge_webhooks
              .upsert(req.body)
              .then(v => res.redirect(`/app/challenge/${challengeId}/settings?updated=true`))
              .catch((e) => { 
                res.redirect(`/app/challenge/${challengeId}/settings?generalerror=true&title=Error updating the notification record`)
                console.error(e);
              });
          } catch (error) {
            logger.error(error);
            res.redirect(
              `/app/challenge/${challengeId}/settings?updated=error`
            );
          }
        } else {
          res.redirect("/app?auth=no");
        }
      } else {
        logger.error("User not logged in POST /challenge/:id");
        res.status(400).send("Sorry, you are not logged in");
      }
    }
  );

  router.post(
    "/challenge/:id/testWebhook",
    requiresAuth(),
    async (req, res) => {
      const openIDuser = await req.oidc.fetchUserInfo();
      const thisUser = await db.users.getOrCreateUser(openIDuser);
      userId = thisUser.id;
      const challengeId = req.params.id;

      if (userId) {
        const userChallenge = await db.challenges.userChallenge(
          challengeId,
          userId
        );
        if (userChallenge.isAdmin || thisUser.super_admin) {
          PubSub.publish("testWebhook", { challengeId });
          res.send("ok");
        } else {
          res.redirect("/app?auth=no");
        }
      }
    }
  );
};
