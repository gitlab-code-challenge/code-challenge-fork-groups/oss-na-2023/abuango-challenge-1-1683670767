var express = require("express");
var router = express.Router();
const { requiresAuth } = require("express-openid-connect");

const db = require("../../db");
const { logger } = require("../../lib/logger");

/* GET app page. */
router.get("/*", requiresAuth(), async (req, res, next) => {
  let { token_type, access_token, isExpired, refresh } = req.oidc.accessToken;
  if (isExpired()) {
    logger.info("Refreshing token");
    ({ access_token } = await refresh());
  }
  const openIDuser = await req.oidc.fetchUserInfo();
  const thisUser = await db.users.getOrCreateUser(openIDuser);
  req.currentUser = thisUser;
  next();
});

router.get("/", requiresAuth(), async (req, res) => {
  logger.debug("Current User", req.currentUser);
  const challenges = await db.challenges.getDetails(req.currentUserId);
  res.render("app/home", {
    currentUser: req.currentUser,
    challenges,
    myChallenges: challenges.filter((x) => x.role && x.active),
    activeChallenges: challenges.filter((x) => x.active),
    previousChallenges: challenges.filter((x) => !x.active && x.role),
  });
});

require("./challenges")(router);
require("./challenges_post")(router);
require("./challenges_delete")(router);
require("./challenges_put")(router);
require("./user")(router);

router.get("/settings", requiresAuth(), async (req, res) => {
  logger.debug(req.currentUser);
  res.send("Settings page");
});

module.exports = router;
