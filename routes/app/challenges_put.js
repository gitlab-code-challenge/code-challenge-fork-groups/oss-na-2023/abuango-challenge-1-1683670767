const { requiresAuth } = require("express-openid-connect");

const db = require("../../db");
const { logger } = require("../../lib/logger");

module.exports = (router) => {
  router.put(
    "/challenge/:challenge_id/completed",
    requiresAuth(),
    async (req, res) => {
      const openIDuser = await req.oidc.fetchUserInfo();
      const thisUser = await db.users.getOrCreateUser(openIDuser);
      requestUser = thisUser.id;

      const challengeId = req.params.challenge_id;
      const { userId, levelId, ref } = req.query;
      if (userId && challengeId && levelId) {
        const userChallenge = await db.challenges.userChallenge(
          challengeId,
          requestUser
        );
        if (userChallenge.isAdmin || req.currentUser.super_admin) {
          db.scoring
            .addCompletion(levelId, challengeId, userId, decodeURI(ref), false)
            .then((ret) => {
              logger.debug(`Added ${ret} record to levels_completed`);
              res.send("Ok");
            })
            .catch((e) => {
              console.error(e);
              res.status(500).json(e);
            });
        } else {
          res.status(500).send("User is not admin who tried to add");
        }
      } else {
        logger.error(
          "User not logged in PUT /challenge/:challenge_id/completed"
        );
        res.status(400).send("Sorry, you are not logged in");
      }
    }
  );

  router.put(
    "/challenge/:challenge_id/promote/:user_id",
    requiresAuth(),
    async (req, res) => {
      const openIDuser = await req.oidc.fetchUserInfo();
      const thisUser = await db.users.getOrCreateUser(openIDuser);
      requestUser = thisUser.id;

      const challengeId = req.params.challenge_id;
      const userToPromote = req.params.user_id;
      if (requestUser && challengeId && userToPromote) {
        const userChallenge = await db.challenges.userChallenge(
          challengeId,
          requestUser
        );
        if (userChallenge.isAdmin || req.currentUser.super_admin) {
          db.users_challenges
            .promoteEntrant(challengeId, userToPromote)
            .then((ret) => {
              res.send("Ok");
            })
            .catch((e) => {
              console.error(e);
              res.status(500).json(e);
            });
        } else {
          res.status(500).send("User is not admin who tried to add");
        }
      } else {
        logger.error(
          "User not logged in PUT /challenge/:challenge_id/completed"
        );
        res.status(400).send("Sorry, you are not logged in");
      }
    }
  );
};
