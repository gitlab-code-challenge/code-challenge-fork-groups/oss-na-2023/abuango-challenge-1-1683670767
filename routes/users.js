var express = require("express");
var router = express.Router();

const db = require("../db");

router.get("/:user_id", async function (req, res, next) {
  const userId = req.params.user_id;
  const thisUser = await db.users.getByGitLabHandle(userId);
  if (thisUser) {
    const badges = await db.user_badges.getByUserId(thisUser.id);
    res.render("users/publicprofile", {
      user: thisUser,
      badges,
    });
  } else {
    res.status(404);
    next({ message: "That user was not found" });
  }
});

router.get("/:user_id/challenges", async function (req, res) {
  const userId = req.params.user_id;
  const thisUser = await db.users.getByGitLabHandle(userId);
  res.render("users/publicchallenges", {
    user: thisUser,
  });
});

router.get("/:user_id/activity", async function (req, res) {
  const userId = req.params.user_id;
  const thisUser = await db.users.getByGitLabHandle(userId);
  res.render("users/publicactivity", {
    user: thisUser,
  });
});

module.exports = router;
