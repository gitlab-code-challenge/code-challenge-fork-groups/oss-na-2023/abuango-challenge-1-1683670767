var express = require("express");
var router = express.Router();


const demopeople = require("../data/demopeople.json");
const db = require("../db");
const { logger } = require("../lib/logger");

/* GET home page. */
router.get("/", async function (req, res, next) {
  let title = "Code Challenge [dot] dev";
  logger.info("Homepage load");
  db.challenges
    .getFeaturedChallenge()
    .then((featuredChallenge) => {
      if (featuredChallenge) {
        logger.info("Showing featured challenge");
        res.render("index", {
          title: `${title} | Featuring the ${featuredChallenge.name} Challenge`,
          featuredChallenge,
        });
      } else {
        logger.info("No featured challenge");
        res.render("index", { title });
      }
    })
    .catch((e) => {
      logger.error("Error getting featured challenge", e);
      res.render("index", { title });
    });
});

router.get("/signup", async (req, res, next) => {
  res.render("signup");
});

router.get("/_ah/warmup", async (req, res) => {
  await db.info.getTime();
  res.send("Ok");
});


require("./invites")(router);
require("./leaderboard")(router);
require("./tasks")(router);

router.get("/demo", function (req, res, next) {
  res.render("demo", {
    title: "Demo | Code Challenge [dot] dev",
    hideApp: true,
    people: demopeople,
  });
});

router.get("/branddemo", function (req, res, next) {
  res.render("branddemo", { title: "Brand Demo" });
});

module.exports = router;
