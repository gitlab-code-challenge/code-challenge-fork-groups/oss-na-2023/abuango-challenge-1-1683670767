var naturalCompare = require("natural-compare-lite");


const db = require("../../db");
const { checkLevels, updateCompleted } = require("../../lib/cron/scoring");
const pkgjson = require("../../package.json");

module.exports = (router) => {
  router.get("/health", async (req, res) => {
    res.send("It was rare, I was there, I remember it all too well");
  });

  router.get("/version", async (req, res) => {
    res.send(pkgjson.version);
  });

  

  router.get("/db", async (req, res) => {
    const migrations = await db.info.getMigrations();
    migrations.sort((a, b) => naturalCompare(a.name, b.name));
    migrations.reverse();
    const now = await db.info.getTime();
    res.json({
      now: now[0].now,
      lastMigration: migrations[0].name,
      migrations,
    });
  });

  router.get(
    "/public/update/challenge/:challenge_id",
    async (req, res, next) => {
      const challenge = await db.challenges.getById(req.params.challenge_id);
      await checkLevels(req.params.challenge_id, challenge.project_name);
      await updateCompleted(req.params.challenge_id);
      res.send("Ok");
    }
  );
};
