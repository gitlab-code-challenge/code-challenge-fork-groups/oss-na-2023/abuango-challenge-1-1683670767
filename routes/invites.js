const { requiresAuth } = require("express-openid-connect");

const db = require("../db");
const { logger } = require("../lib/logger");
const { isEnabled } = require("../lib/unleash");

module.exports = (router) => {
  router.get("/j/:challenge_shortname", async (req, res) => {
    const showInvitePage = isEnabled('show-invite-page');
    const c = await db.challenges.getByShortName(
      req.params.challenge_shortname
    );
    if (c) {
      if (showInvitePage) {
        const challenge = await db.challenges.getById(c.id);
        res.render('invite', {
          challenge,
        })
      } else {
        logger.info(
          `Redirecting from join link ${req.params.challenge_shortname} to challenge ID ${c.id}`
        );
        res.redirect(`/app/challenge/${c.id}?frominvite=true`);
      }
    } else {
      logger.error(
        `No redirect found for shortname ${req.params.challenge_shortname}`
      );
      res.redirect("/?error=notfound");
    }
  });
};
