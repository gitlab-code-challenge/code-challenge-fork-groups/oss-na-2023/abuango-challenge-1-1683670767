var express = require("express");
var router = express.Router();
const { format } = require("util");
const Multer = require("multer");
const { Storage } = require("@google-cloud/storage");

const storage = new Storage();
const bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET);
const db = require("../db");

const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 25 * 1024 * 1024, // no larger than 25mb, you can change as needed.
  },
});

router.post("/upload", multer.single("file"), async (req, res, next) => {
  if (!req.file) {
    res.status(400).send("No file uploaded.");
    return;
  }

  // Create a new blob in the bucket and upload the file data.
  const blob = bucket.file(req.file.originalname);
  const blobStream = blob.createWriteStream();

  blobStream.on("error", (err) => {
    next(err);
  });

  blobStream.on("finish", () => {
    // The public URL can be used to directly access the file via HTTP.
    const publicUrl = format(
      `https://storage.googleapis.com/${bucket.name}/${blob.name}`
    );
    if (req.body.saveto) {
      switch (req.body.saveto) {
        case "levelBadge":
          const resp = db.levels.saveBadge(
            req.body.levelId,
            publicUrl,
            req.body.challengeId
          );
          res.redirect(
            `/app/challenge/${req.body.challengeId}/level/${req.body.levelId}?updated=true`
          );
          break;
        default:
          res.status(200).send(publicUrl);
          break;
      }
    } else {
      res.status(200).send(publicUrl);
    }
  });

  blobStream.end(req.file.buffer);
});

module.exports = router;
