const db = require("../db");
const { logger } = require("../lib/logger");
const {
  userLevelDetails,
  levelDetails,
  byTimeOfCompletion,
} = require("../lib/scoreboard");
const axios = require('axios').default;
const { Readable } = require('stream');


module.exports = (router) => {
  router.get("/leaderboard/:id", function (req, res) {
    try {
      const challengeId = req.params.id;
      let qr_image = '';
     
      const promises = [
        db.challenges.getById(challengeId),                           // data[0]
        db.challenges.userChallenge(challengeId, req.currentUserId),  // data[1]
        db.users_challenges.getEntrants(challengeId),                 // data[2]
        db.levels.getAllByChallengeId(challengeId),                   // data[3]
        db.scoring.getCompleteByChallenge(challengeId),               // data[4]

      ];

      db.challenges.getlbQRContent(challengeId).then(function (qrContentData ){
        
        const qrContent = qrContentData && qrContentData.trim() !== ""  ? qrContentData : 'https://codechallenge.dev/app/challenge/'+challengeId;
    
        axios.get(
        'https://qrcode-generator-cimwkqo2uq-ez.a.run.app/?inner_dots_color=171321&inner_dots_type=dots&corners_square_type=extra-rounded&corners_square_color=171321&corners_inner_type=dots&corners_inner_color=171321&inner_image_url=gitlab-black&data='+encodeURIComponent(qrContent),
          {
            headers: {
              'Authorization': 'Bearer '+process.env.QR_API_KEY
            }
          }
        ).then(function (response) {
          
          qr_image = response.data;

          

          Promise.all(promises)
            .then((data) => {
              const entrants = data[2];
              const levels = data[3];
              const completed = data[4];
              
              res.render("leaderboard", {
                hideApp: true,
                challengeId,
                currentUser: req.currentUser,
                challenge: data[0],
                userChallenge: data[1],
                entrants: data[2],
                levels: data[3],
                completed: data[4],
                qr_code: qr_image,
                userLevelDetails: userLevelDetails(entrants, levels, completed),
                levelDetails: levelDetails(entrants, levels, completed).reverse(),
                byTime: byTimeOfCompletion(entrants, levels, completed).slice(0, 100),
              });
            })
            .catch((err) => console.error(err));
        })
        .catch(function (error) {
          console.log(error);
        });
      });
      
    } catch (error) {
      console.error(error);
      res.status(500).send("An error occured.  Please try again");
    }
  });

  router.get("/leaderboard/:id/signups", async function(req, res) {
    try {
      const challengeId = req.params.id;
      const signups = await db.signups.getAllByChallengeId(challengeId);
      const challenge = await db.challenges.getById(challengeId);

      res.render("signups", {
        hideApp: true,
        challengeId,
        challenge,
        signups,
      })
    } catch (error) {
      console.error(error);
      res.status(500).send("An error occured.  Please try again.");    
    }
  });

  router.get("/leaderboard/:id/signups.json", async function(req, res) {
    try {
      const challengeId = req.params.id;
      const signups = await db.signups.getAllByChallengeId(challengeId);

      res.json(signups);
    } catch (error) {
      console.error(error);
      res.status(500).send("An error occured.  Please try again.");    
    }
  });
};
