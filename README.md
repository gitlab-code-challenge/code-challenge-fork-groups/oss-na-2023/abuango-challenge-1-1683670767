# Code Challenge

## Development

### Getting Started

1. Copy `.env.example` to `.env` and set the proper settings there
1. Run `npm install`

### Running in Development

`npm run dev`

or for Standalone mode

`STANDALONE=true npm run dev`

## Run Standalone

To run in standalone mode (without login or a database):

1. Run `npm run build`
1. TBD: Place this `JSON` file
1. Run `npm run standalone`

## Production

For notes on the production architecture, see [ARCHITECTURE.md](architecture.md)

Production runs in GCP in the GitLab `group-community` environment. Notes from setup of that environment are below.

1. Run in App Engine, as the default service in the account
1. Cloud SQL Postgres for the database
1. Secert Manager to communicate the database password to App Engine
1. DNS managed in the same project
1. Granted the default service account granted additinoal roles of: `Cloud SQL Client`

### Production Migrations

1. Install @databasees/pg-migrations globally `yarn global add @databases/pg-migrations`
1. Enable access from your IP address by authenticating with `gcloud sql connect codechallenge --user=postgres`
1. Get the Public IP for your SQL instance
1. Run migration by using the `--database` flag like

```bash
pg-migrations apply --directory ./db/migrations --database "postgresql://postgres:URL_ENCODED_PASSWORD@1.1.1.1/postgres"
```

### Test Migration

pg-migrations apply --directory ./db/migrations --database "postgresql://postgres@127.0.0.1/test-db"

### app.yaml

To deploy to GCP, an app.yaml is required. You can copy the `app.yaml.safe` as an example. In production, we create the app.yaml in CI using the `$APP_YAML` CI variable, stored as a base64 representation of the yaml file.

To update the CI yaml file:

1. Copy the current contents of `$APP_ENV_YAML`
1. `echo -n "<paste>" | base64 --decode > app_env.yaml`
1. Edit as needed
1. `cat app_env.yaml | base64 | copy` (or `pbcopy` on macOS)
1. Paste over `$APP_ENV_YAML`
1. Redeploy

## Database

To create the `database.uml` file using [Planter]() run `./dbdiagram.sh`

## Documentation

The documentation project is a separate project stored under the `/docs` folder

### Developing the Docmentation

1. Enter the docs folder with `cd docs`
1. `npm install`
1. `npm run dev`
1. The docs are available at `http://localhost:3000`

### Deploying the Documentation

The documentation gets deployed to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) and can be found at [docs.codechallenge.dev](https://docs.codechallenge.dev/en/introduction/).

## Tools

1. Express.js
1. Handlebars (for templating)
1. Bulma & additions (for SCSS/CSS)
1. [express-openid-connect](https://github.com/auth0/express-openid-connect) for OpenID with GitLab
1. @databases/pg for Postgresql

### Docs Tooling

1. [Astro](https://astro.build/)
