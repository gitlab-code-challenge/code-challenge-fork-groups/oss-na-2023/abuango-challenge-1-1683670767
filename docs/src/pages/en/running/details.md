---
title: Challenge Details
description: Challenge Details
layout: ../../../layouts/MainLayout.astro
---

Information on the Challenge Details page is coming soon 🙂

## Challenge Details

## Learn More

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
