---
title: Levels
description: Levels
layout: ../../../layouts/MainLayout.astro
---

Information on creating and running Code Challenges is coming soon 🙂

## Types of Levels

## Creating a Level

## Learn More

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
