---
title: Signups
description: Signups
layout: ../../../layouts/MainLayout.astro
---

## Signups Page

In addition to the [leaderboard](/en/leaderboards), each challenge has a list of all signups. This includes the user's name at the registration time, their rules agreement, and if they completed any challenges.

This page allows teams to compile a winner list by providing the following data quickly:

- **Signup ID**: Unique ID of the signup
- **User ID**: The system user ID for this user
- **Name**: The provided name (prompts the user for first name, last initial)
- **Rules Agreement**: If there were rules for this challenge, this corresponds to the user checking the "I agree" rules checkbox
- **Winner**: Did the user complete ANY level in the challenge. For details on level by level, see the [challenge details](/en/running/details) page.
- **Created At**: A timestamp of when the signup record was created.

## Accessing the Signups Page

The signups page isn't linked from within the app, but it is located under the leaderboard. So if you navigate to the leaderboard, and then add `/signups` to the URL, you'll get the sign up page.

For example, the URL for challenge #4 would be

```
https://codechallenge.dev/leaderboard/4/signups
```

### Plain Text Version

For a plain text version of the signup list with only those who have completed levels, you can run something like:

```bash
    curl -s https://codechallenge.dev/leaderboard/4/signups.json | \
    jq '.[] | select(.levels_completed?) | .lastname_fi ' | \
    sed -ne 's/.*/- &/p' | tr -d '"' | \
    copy
```

## For Developers

These records are stored in the `signups` table.

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge), we'd greatly accept your merge requests! 🥰
