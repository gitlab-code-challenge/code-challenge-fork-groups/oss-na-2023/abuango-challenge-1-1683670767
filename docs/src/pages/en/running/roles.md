---
title: Challenge Roles
description: Challenge Roles
layout: ../../../layouts/MainLayout.astro
---

Information on challenge roles is coming soon 🙂

## Roles

### Owner

### Admin

### Entrant

## Changing Roles

## Learn More

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
