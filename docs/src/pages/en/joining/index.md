---
title: Joining Code Challenges
description: Joining Code Challenges
layout: ../../../layouts/MainLayout.astro
setup: |
    import DocImage from '../../../components/DocImage.astro'
---

## Finding a Challenge

To join a challenge, you must first find the challenge you want to join. To do this, you can either log into `codechallenge.dev` and look under **Active Challenges** or get a join link from the challenge host. Join links are of the form:

> `https://codechallenge.dev/j/kubeconeu`

Where `kubeconeu` is the shortcode for the challenge.

<DocImage src="/img/active_challenges.png" alt="Active Challenges screen" />

## Joining a Challenge

Once you've chosen a challenge, you can join the challenge from the challenge page join button. If you were sent the join link from a host, you'd automatically be prompted to enter the challenge once you log in.

<DocImage src="/img/join_button.png" alt="Join challenge button" />

## Learn More

And that's in! You're in and added as an entrant to the challenge. You can now start completing [levels](/en/joining/completion#completing-levels) to get listed on the [leaderboard](/en/leaderboards)! To learn more about completing challenges, see [Completion](/en/joining/completion).

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
