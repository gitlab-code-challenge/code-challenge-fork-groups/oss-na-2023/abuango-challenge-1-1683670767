---
title: Completing Levels
description: Completing Levels
layout: ../../../layouts/MainLayout.astro
setup: |
    import VideoOverview from '../../../components/VideoOverview.astro'
    import DocImage from '../../../components/DocImage.astro'
---

## Completing Levels
To complete a level, follow the instructions given by the challenge host. A given level may involve making a merge request, creating an issue, or completing other activities inside or outside of GitLab. Typically this starts with [forking](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the provided project and then follow the steps there.

For example, if the level calls for you to create a merge request to the sample project, the steps to complete the level would be:

1. Fork the project (or open your existing fork)
1. Make the change (in the WebIDE or elsewhere)
1. Commit the change to a branch on your fork
1. Open a merge request against the *source* project to merge your forked branch back into the sample project

The video below shows these steps in detail:

<VideoOverview />

## Scoring
For most levels, scoring is handled automatically when `CodeChallenge.dev` queries GitLab for the current state of the related projects. This process could take up to 10 minutes, but if you've correctly completed the level, you'll see "Incomplete" change to a checkmark with the time that you met the challenge noted and you'll see your avatar on the [leaderboard](/en/leaderboards).

<DocImage src="/img/challenge_completion.png" alt="Showing a incomplete and completed challenge to note the differences" />

## Learn More
- Learn about the [types of levels](/en/running/levels#types-of-levels)
- Learn about [leaderboards](/en/leaderboards)

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
