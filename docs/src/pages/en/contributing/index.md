---
title: Contributing to CodeChallenge.dev
description: Contributing to CodeChallenge.dev
layout: ../../../layouts/MainLayout.astro
---

Information on contributing to CodeChallenge.dev is coming soon 🙂

## Prerequisites

## Repository

## Development Environmnet

## Learn More

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
