---
title: Self-hosting the CodeChallenge software
description: Self-hosting the CodeChallenge software
layout: ../../../layouts/MainLayout.astro
---

Information on self-hosting the CodeChallenge software is coming soon 🙂

## Environment Requirements

## Learn More

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
