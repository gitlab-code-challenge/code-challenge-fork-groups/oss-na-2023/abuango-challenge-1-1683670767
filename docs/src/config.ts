export const SITE = {
	title: 'CodeChallenge.dev Documentation',
	description: 'Code Challenges are designed to help you learn new skills, explore GitLab, or level up your DevOps game. Complete the various levels either for prizes or just for bragging rights!',
	defaultLanguage: 'en_US',
};

export const OPEN_GRAPH = {
	image: {
		src: 'https://github.com/withastro/astro/blob/main/assets/social/banner.jpg?raw=true',
		alt:
			'astro logo on a starry expanse of space,' +
			' with a purple saturn-like planet floating in the right foreground',
	},
	twitter: 'astrodotbuild',
};

export const KNOWN_LANGUAGES = {
	English: 'en',
};

// Uncomment this to add an "Edit this page" button to every page of documentation.
export const GITLAB_EDIT_URL = `https://gitlab.com/gitlab-de/codechallenge/-/tree/main/docs/`;

// Uncomment this to add an "Join our Community" button to every page of documentation.
// export const COMMUNITY_INVITE_URL = `https://astro.build/chat`;

// Uncomment this to enable site search.
// See "Algolia" section of the README for more information.
// export const ALGOLIA = {
//   indexName: 'XXXXXXXXXX',
//   appId: 'XXXXXXXXXX',
//   apiKey: 'XXXXXXXXXX',
// }

export const SIDEBAR = {
	en: [
		{ text: '', header: true },
		{ text: '🏁 Getting Started', header: true },
		{ text: 'Introduction', link: 'en/introduction' },
		{ text: 'Joining a Challenge', link: 'en/joining' },
		{ text: 'Completing Levels', link: 'en/joining/completion' },
		{ text: 'Leaderboards', link: 'en/leaderboards' },

		{ text: '🚧 Running Challenges', header: true },
		{ text: 'Run your own challenge', link: 'en/running' },
		{ text: 'Levels', link: 'en/running/levels' },
		{ text: 'Details', link: 'en/running/details' },
		{ text: 'Roles', link: 'en/running/roles' },
		{ text: 'Signups', link: 'en/running/signups' },
		{ text: 'Settings', link: 'en/running/settings' },

		{ text: '🚧 Contributing', header: true },
		{ text: 'Contributing Code', link: 'en/contributing' },
		{ text: 'Contributing Docs', link: 'en/contributing/docs' },

		{ text: '🚧 Self Hosting', header: true },
		{ text: 'Code Challenge Server', link: 'en/self-hosting' },
	],
};
