```plantuml
hide circle
skinparam linetype ortho

entity "**challenges**" {
  + ""id"": //serial [PK]//
  --
  *""name"": //character varying(40) //
  ""description"": //text //
  ""start_date"": //date //
  *""active"": //boolean //
  ""project_name"": //text //
  ""project_id"": //integer //
  *""created_at"": //timestamp with time zone //
  *""updated_at"": //timestamp with time zone //
  ""end_date"": //date //
  ""created_by"": //integer [FK]//
  ""shortname"": //character varying(40) //
}

entity "**deleted_challenges**" {
  --
  ""id"": //integer //
  ""name"": //character varying(40) //
  ""description"": //text //
  ""start_date"": //date //
  ""active"": //boolean //
  ""project_name"": //text //
  ""project_id"": //integer //
  ""created_at"": //timestamp with time zone //
  ""updated_at"": //timestamp with time zone //
  ""end_date"": //date //
  ""created_by"": //integer //
  ""shortname"": //character varying(40) //
}

entity "**deleted_users_challenges**" {
  --
  ""id"": //integer //
  ""user_id"": //integer //
  ""challenge_id"": //integer //
  ""role"": //character varying(10) //
  ""created_at"": //timestamp with time zone //
  ""updated_at"": //timestamp with time zone //
}

entity "**glprojects**" {
  + ""id"": //integer [PK]//
  --
  *""name"": //character varying(100) //
  ""name_with_namespace"": //text //
  *""path"": //character varying(100) //
  ""path_with_namespace"": //text //
  ""default_branch"": //character varying(100) //
  ""http_url_to_repo"": //text //
  ""web_url"": //text //
  ""avatar_url"": //text //
  ""forks_count"": //integer //
  ""star_count"": //integer //
  ""visibility"": //character varying(40) //
  ""description"": //text //
  *""created_at"": //timestamp with time zone //
  *""updated_at"": //timestamp with time zone //
}

entity "**levels**" {
  + ""id"": //serial [PK]//
  --
  *""challenge_id"": //integer [FK]//
  *""level"": //integer //
  *""title"": //character varying(250) //
  ""description"": //text //
  *""type"": //character varying(40) //
  ""details"": //json //
  *""created_at"": //timestamp with time zone //
  *""updated_at"": //timestamp with time zone //
  ""after"": //character varying(100) //
  ""contains"": //text //
  ""created_by"": //integer //
  ""completed"": //integer //
}

entity "**levels_completed**" {
  + ""level_id"": //integer [PK][FK]//
  + ""challenge_id"": //integer [PK][FK]//
  + ""user_id"": //integer [PK][FK]//
  --
  ""started"": //boolean //
  *""start_time"": //timestamp with time zone //
  ""completed"": //boolean //
  ""complete_time"": //timestamp with time zone //
  *""created_at"": //timestamp with time zone //
  *""updated_at"": //timestamp with time zone //
  ""ref"": //text //
}

entity "**users**" {
  + ""id"": //serial [PK]//
  --
  *""name"": //character varying(200) //
  *""gitlab_handle"": //character varying(200) //
  *""gitlab_id"": //integer //
  ""email"": //text //
  ""picture"": //text //
  *""last_login"": //timestamp with time zone //
  *""created_at"": //timestamp with time zone //
  *""updated_at"": //timestamp with time zone //
}

entity "**users_challenges**" {
  + ""id"": //serial [PK]//
  --
  *""user_id"": //integer [FK]//
  *""challenge_id"": //integer [FK]//
  *""role"": //character varying(10) //
  *""created_at"": //timestamp with time zone //
  *""updated_at"": //timestamp with time zone //
}

"**challenges**"   }--  "**users**"

"**levels**"   }--  "**challenges**"

"**levels_completed**"   }--  "**challenges**"

"**levels_completed**"   }--  "**levels**"

"**levels_completed**"   }--  "**users**"

"**users_challenges**"   }--  "**challenges**"

"**users_challenges**"   }--  "**users**"
```
