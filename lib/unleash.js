const unleash = require('unleash-client');

unleash.initialize({
  url: 'https://gitlab.com/api/v4/feature_flags/unleash/34672937',
  instanceId: 'msps3vp5RHYDgFLa22ui',
  appName: 'production',
//   environment: process.env.APP_ENV,
//   customHeaders: { Authorization: 'SOME-SECRET' },
});

const isEnabled = unleash.isEnabled;

module.exports = {
    isEnabled,
}

