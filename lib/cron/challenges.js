const PubSub = require("pubsub-js");
const debug = require("debug")("codingchallenge:cron");

const db = require("../../db");

async function updateAllProjectInfo() {
    debug("Running updateAllProjectInfo");
    const allChallenges = await db.challenges.getAll();
    allChallenges.forEach(challenge => {
        PubSub.publish("updateChallenge", { challengeId: challenge.id, userId: null, source: 'cron.challenges.updateAllProjectInfo' });
    });
}


module.exports = { updateAllProjectInfo }