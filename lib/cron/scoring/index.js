const PubSub = require("pubsub-js");
const debug = require("debug")("codingchallenge:scoring");

const db = require("../../../db");
const { logger } = require("../../logger");
const mergeRequests = require("./merge_requests");
const pipeline = require("./pipeline");

async function checkScores() {
  const activeChallenges = await db.challenges.getAll();
  for (const challenge of activeChallenges) {
    await checkLevels(challenge.id, challenge.project_name);
    await updateCompleted(challenge.id);
  }
}

async function checkLevels(challengeId, gitlab_project) {
  debug(`Score levels for challenge ${challengeId} (${gitlab_project})`);
  const theseLevels = await db.levels.getAllByChallengeId(challengeId);
  theseLevels.forEach((l) => {
    switch (l.type) {
      case "MR":
        debug(`Level ID ${l.id}: Merge request type`);
        mergeRequests.check(l, challengeId);
        break;
      case "PIPELINE":
        debug(`Level ID ${l.id}: Pipeline type`);
        pipeline.check(l, challengeId);
      case "MANUAL":
        debug(`Level ID ${l.id} is manually verified.  Skipping`);
        break;
      default:
        debug(`Type not implemented: ${l.type}`);
        logger.error(`Type not implemented: ${l.type}`);
        break;
    }
  });
}

async function updateCompleted(challengeId) {
  debug(`Update completed levels for challenge ${challengeId}`);
  const stats = await db.scoring.getLevelStats(challengeId);
  for (const level of stats) {
    db.scoring.updateLevelCompleted(level.level_id, level.completed);
  }
}

async function awardBadge(challengeId, levelId, userId, ref) {
  const challenge = await db.challenges.getById(challengeId);
  const level = await db.levels.getById(levelId);

  if (level.badge) {
    db.user_badges
      .add(
        userId,
        challengeId,
        levelId,
        level.badge,
        ref,
        challenge.name,
        level.title,
        level.level
      )
      .then((v) => debug(`Badge awarded to ${userId} for level ${levelId}`));
  }
}

async function revokeBadge(challengeId, levelId, userId) {
  db.user_badges
    .remove(userId, challengeId, levelId)
    .then((v) => debug(`Badge revoked from ${userId} for level ${levelId}`));
}

PubSub.subscribe("levelCompleted", (msg, data) => {
  const { levelId, challengeId, userId, ref } = data;
  awardBadge(challengeId, levelId, userId, ref);
  updateCompleted(challengeId);
});
PubSub.subscribe("levelCompletedDeleted", (msg, data) => {
  const { levelId, challengeId, userId } = data;
  revokeBadge(challengeId, levelId, userId);
  updateCompleted(challengeId);
});

module.exports = {
  checkScores,
  checkLevels,
  updateCompleted,
};
