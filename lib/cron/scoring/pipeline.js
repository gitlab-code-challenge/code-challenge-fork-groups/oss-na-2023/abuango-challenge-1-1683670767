const PubSub = require("pubsub-js");
const log = require("debug")("codingchallenge:scoring");
const debug = require("debug")("codingchallenge:scoringdebug");

const db = require("../../../db");
const api = require("../../gitlab/api");
const { logger } = require("../../logger");

/**
 *
 * @param {import('../../../db/model').levels.Level} level The details for the level to be checked
 * @param {number} challengeId The ID for this challenge
 */
async function check(level, challengeId) {
  log(`PIPELINE check start. Level ${level.id} (challenge ${challengeId})`);
  const level_id = level.id;

  const challenge = await db.challenges.getById(challengeId);
  const entrants = await db.users_challenges.getEntrants(challengeId);
  const completed = await db.scoring.getComplete(level_id);

  log(`There are ${entrants.length} total entrants`);
  const toCheck = entrants.filter((e) => {
    if (completed.find((v) => v.user_id === e.user_id)) {
      return false;
    }
    return true;
  });
  log(`${toCheck.length} entrants have not completed the challenge`);

  toCheck.forEach(async (c) => {
    const { user_id, gitlab_handle } = c;
    const { project_name } = challenge;
    let isComplete = false;
    let ref = "";
    /**
     * Check logic
     *
     * 1. Get all merge requests for this user on the project
     * 2. For each merge request, get the pipelines for that merge request
     * 3. If that pipeline (e.g. ANY pipeline) is has a status of "success" then the level is complete
     */
    let apiRes = await api.merge_requests.byProjectByAuthor(
      project_name,
      gitlab_handle,
      level.after
    );

    log(
      `Found ${apiRes.length} MRs to check for ${gitlab_handle} in ${project_name}`
    );

    let i = 0;
    for (const mr of apiRes) {
      debug(`MR ${i} of ${apiRes.length}`);
      // Find the pipeline for this merge request
      let mrPipelines = await api.pipelines.getByMR(project_name, mr.iid);
      for (const pipeline of mrPipelines) {
        debug(`Pipeline ${pipeline.id} has a status of ${pipeline.status}`);
        if (pipeline.status === "success") { 
          isComplete = true;
          ref = pipeline.web_url;
        }
      }
    }

    if (isComplete) {
      db.scoring.addCompletion(level_id, challengeId, user_id, ref);
    }

    log(
      `Finished.  ${gitlab_handle} ${
        isComplete ? "has" : "has NOT"
      } completed level ID ${level_id} (TYPE: ${level.type})`
    );
  });
}

module.exports = {
  check,
};
