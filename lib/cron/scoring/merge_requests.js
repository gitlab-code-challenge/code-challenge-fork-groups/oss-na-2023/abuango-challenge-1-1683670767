const PubSub = require("pubsub-js");
const log = require("debug")("codingchallenge:scoring");
const debug = require("debug")("codingchallenge:scoringdebug");

const db = require("../../../db");
const api = require("../../gitlab/api");
const { logger } = require("../../logger");

/**
 *
 * @param {import('../../../db/model').levels.Level} level The details for the level to be checked
 * @param {number} challengeId The ID for this challenge
 */
async function check(level, challengeId) {
  log(`MR check start. Level ${level.id} (challenge ${challengeId})`);
  const level_id = level.id;

  const challenge = await db.challenges.getById(challengeId);
  const entrants = await db.users_challenges.getEntrants(challengeId);
  const completed = await db.scoring.getComplete(level_id);

  log(`There are ${entrants.length} total entrants`);
  const toCheck = entrants.filter((e) => {
    if (completed.find((v) => v.user_id === e.user_id)) {
      return false;
    }
    return true;
  });
  log(`${toCheck.length} entrants have not completed the challenge`);

  toCheck.forEach(async (c) => {
    const { user_id, gitlab_handle } = c;
    const { project_name } = challenge;
    let isComplete = false;
    let ref = "";
    let apiRes = await api.merge_requests.byProjectByAuthor(
      project_name,
      gitlab_handle,
      level.after
    );

    log(
      `Found ${apiRes.length} MRs to check for ${gitlab_handle} in ${project_name}`
    );

    if (level.contains) {
      // Start with assumption it is false
      isComplete = false;
      debug(
        `Checking ${apiRes.length} MRs to see if any changes contain ${level.contains}`
      );
      let i = 0;
      for (const mr of apiRes) {
        i++;
        debug(`MR ${i} of ${apiRes.length}`);
        const theseChanges = await api.merge_requests.mergeRequestChanges(
          mr.project_id,
          mr.iid
        );

        if (theseChanges.changes) {
          debug(`${theseChanges.changes.length} changes in this MR`);
          let j = 0;
          for (const change of theseChanges.changes) {
            j++;
            debug(`Change ${j} of ${theseChanges.changes.length}`);
            if (change.diff.includes(level.contains)) {
              ref = mr.web_url;
              isComplete = true;
              break;
            }
          }
          if (isComplete) break;
        }
      }
    } else {
      if (apiRes.length > 0) ref = apiRes[0].web_url;
      isComplete = apiRes.length > 0;
    }

    if (isComplete) {
      db.scoring.addCompletion(level_id, challengeId, user_id, ref);
    }

    log(
      `Finished.  ${gitlab_handle} ${
        isComplete ? "has" : "has NOT"
      } completed level ID ${level_id} (TYPE: ${level.type})`
    );
  });
}

module.exports = {
  check,
};
