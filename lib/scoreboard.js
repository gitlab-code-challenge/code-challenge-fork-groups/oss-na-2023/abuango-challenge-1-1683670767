/**
 *
 * @param {import("../db/model").users_challenges.Entrant[]} entrants A list of entrants for this challenge
 * @param {import("../db/model").levels.Level[]} levels A list of all the levels in this challenge
 * @param {import("../db/model").levels_completed.LevelsCompleted[]} completed All of the completed entries for this challenge
 */
function userLevelDetails(entrants, levels, completed) {
  let ret = [];

  for (const user of entrants) {
    let thisU = user;
    thisU.levels = [];
    for (const level of levels) {
      let thisUserLevel = completed.find(
        (x) => x.level_id === level.id && x.user_id === user.user_id
      );
      thisU.levels.push({
        level: level.level,
        level_id: level.id,
        ...thisUserLevel,
        user_id: user.user_id,
        challenge_id: user.challenge_id,
      });
    }
    thisU.levels.sort((a, b) => (a.level > b.level ? 1 : -1));
    ret.push(thisU);
  }
  return ret;
}

/**
 *
 * @param {import("../db/model").users_challenges.Entrant[]} entrants A list of entrants for this challenge
 * @param {import("../db/model").levels.Level[]} levels A list of all the levels in this challenge
 * @param {import("../db/model").levels_completed.LevelsCompleted[]} completed All of the completed entries for this challenge
 */
function levelDetails(entrants, levels, completed) {
  let ret = [];
  levels.sort((a, b) => (a.level > b.level ? 1 : -1));
  completed.sort((a, b) => (a.complete_time > b.complete_time ? -1 : 1));

  for (const level of levels) {
    let thisLevelCompleted = completed.filter((v) => v.level_id === level.id);
    let thisLevelCompletedDetails = addUserToCompletedRecords(
      thisLevelCompleted,
      entrants
    );
    const thisLevel = {
      ...level,
      thisLevelCompletedDetails,
    };
    ret.push(thisLevel);
  }
  return ret;
}

/**
 *
 * @param {import("../db/model").users_challenges.Entrant[]} entrants A list of entrants for this challenge
 * @param {import("../db/model").levels.Level[]} levels A list of all the levels in this challenge
 * @param {import("../db/model").levels_completed.LevelsCompleted[]} completed All of the completed entries for this challenge
 */
function byTimeOfCompletion(entrants, levels, completed) {
  let ret = [];
  completed.sort((a, b) => (a.complete_time > b.complete_time ? -1 : 1));

  for (const completeRecord of completed) {
    const thisLevel = levels.find((x) => x.id === completeRecord.level_id);
    const thisR = {
      ...completeRecord,
      levelDetails: { ...thisLevel },
    };
    ret.push(thisR);
  }

  ret = addUserToCompletedRecords(ret, entrants);

  return ret;
}

function addUserToCompletedRecords(completed, entrants) {
  let thisLevelCompletedDetails = [];
  for (const completeRecord of completed) {
    const thisU = entrants.find((x) => x.user_id === completeRecord.user_id);
    const thisR = {
      ...completeRecord,
      name: thisU.name,
      gitlab_handle: thisU.gitlab_handle,
      gitlab_id: thisU.gitlab_id,
      email: thisU.email,
      picture: thisU.picture,
      userrole: thisU.role,
    };
    thisLevelCompletedDetails.push(thisR);
  }
  return thisLevelCompletedDetails;
}

module.exports = {
  userLevelDetails,
  levelDetails,
  byTimeOfCompletion,
};
