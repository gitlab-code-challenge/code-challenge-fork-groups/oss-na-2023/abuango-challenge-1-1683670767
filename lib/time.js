const dayjs = require('dayjs');
const localizedFormat = require("dayjs/plugin/localizedFormat");
const relativeTime = require("dayjs/plugin/relativeTime");
const updateLocale = require("dayjs/plugin/updateLocale");
dayjs.extend(localizedFormat);
dayjs.extend(relativeTime);
dayjs.extend(updateLocale);

const iso8601 = (date) => { return dayjs(date).format('YYYY-MM-DD') }

function formatAllDates(arr) {
    arr.forEach(c => {
        if (c.start_date) c.start_date = iso8601(c.start_date);
        if (c.end_date) c.end_date = iso8601(c.end_date);
    });
    return arr;
}

module.exports = { iso8601, formatAllDates, dayjs }