const db = require("../../db");
const groups = require("./api/groups");
const { logger } = require("../logger");
const util = require("util");

async function addUserToGroup(msg, data) {
    logger.info("newEntrant subscripition GitLab handler");
    const { userId, challengeId } = data;
    const userChallenges = await db.challenges.getDetails(userId);
    const thisChallenge = userChallenges.find(x => x.id === parseInt(challengeId));
    
    if (thisChallenge.enable_fork_group) {
        const thisGroupId = thisChallenge.fork_group_id ? thisChallenge.fork_group_id : 0
        logger.info(`Adding user ${userId} to GitLab group ${thisGroupId}`);
        const thisUser = await db.users.getById(userId)
        groups.addUserToGroup(thisGroupId, thisUser.gitlab_id, thisChallenge.fork_group_expiry);
    }
}

async function createUserFork(msg, data) {
    logger.info("newEntrant subscripition GitLab Fork handler");
    const { userId, challengeId } = data;
    const userChallenges = await db.challenges.getDetails(userId);
    const thisChallenge = userChallenges.find(x => x.id === parseInt(challengeId));
    
    if (thisChallenge.enable_fork_group) {
        const thisGroupId = thisChallenge.fork_group_id ? thisChallenge.fork_group_id : 0
        logger.info(`Adding user ${userId} to GitLab fork group ${thisGroupId}`);
        const thisUser = await db.users.getById(userId);
        logger.info("Checking project_name: "+thisChallenge.project_name);
        const thisProject = await db.glprojects.getByNamespace(thisChallenge.project_name);
        //logger.info(util.inspect(thisProject));
        groups.forkProject(thisProject[0].id, thisGroupId, thisUser.id, thisUser.gitlab_id, thisUser.gitlab_handle, thisChallenge.fork_group_expiry, challengeId );
    }
}

module.exports = { addUserToGroup, createUserFork }