const api = require("./api");

async function byProject(projectId) {
  return api.MergeRequests.all({ projectId });
}

async function byProjectByAuthor(projectId, authorUsername, createdAfter) {
  return api.MergeRequests.all({ projectId, authorUsername, createdAfter });
}

async function mergeRequestChanges(projectId, mergeRequestIid) {
  return api.MergeRequests.changes(projectId, mergeRequestIid);
}

module.exports = {
  byProject,
  byProjectByAuthor,
  mergeRequestChanges,
};
