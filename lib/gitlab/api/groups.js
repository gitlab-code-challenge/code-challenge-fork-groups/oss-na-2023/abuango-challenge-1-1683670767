const dayjs = require('dayjs');
const { logger } = require("../../logger");
const api = require("./api");
const db = require("../../../db");
const util = require("util");

/**
 * https://docs.gitlab.com/ee/api/members.html#valid-access-levels 
 * 
 * No access (0)
 * Minimal access (5) (Introduced in GitLab 13.5.)
 * Guest (10)
 * Reporter (20)
 * Developer (30)
 * Maintainer (40)
 * Owner (50) - Only valid to set for groups
*/
async function addUserToGroup(groupId, userId, expiry) {

    api.GroupMembers.add(groupId, userId, 30, {
        expires_at: dayjs().add(expiry, 'day').format('YYYY-MM-DD')
    })
    .catch(e => {
        logger.error("Error adding user to group");
        console.error(e);
    })

    
}

async function forkProject(projectId, namespaceId, userId, gitlabUserId, username, expiry, challengeId) {
    logger.info("Creating fork...for "+projectId+" in challenege "+challengeId);
    await api.Projects.fork(projectId, {namespace_id: namespaceId, name: username+'-challenge-'+Math.floor(Date.now() / 1000), path: username+'-challenge-'+userId+'-'+Math.floor(Date.now() / 1000) })
    .then((value) => { 
        logger.info("Creating fork...");
        const { id, path_with_namespace} = value;
        logger.info("Creating fork..."+path_with_namespace);

        db.glprojects.updateUserProjectFork(path_with_namespace, userId,challengeId).then((value) => {
            logger.info(util.inspect(value));
        }).catch(e => {
            logger.error("Error Update fork details in DB");
            console.error(e);
        });

        api.ProjectMembers.add(id, gitlabUserId, 40, {
            expires_at: dayjs().add(expiry, 'day').format('YYYY-MM-DD')
        })
        .catch(e => {
            logger.error("Error Update fork details in DB");
            console.error(e);
        })
    })
    .catch(e => {
        logger.error("Error Forking project");
        console.error(e);
    })

    

    
}

module.exports = {
    addUserToGroup,
    forkProject
}

