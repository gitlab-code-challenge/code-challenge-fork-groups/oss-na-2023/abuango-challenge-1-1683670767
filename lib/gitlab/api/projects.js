const api = require("./api");

async function getProject(nameOrId) {
  return api.Projects.show(nameOrId);
}

module.exports = { getProject };
