const api = require("./api");

/**
 * Get all of the pipelines for a given merge request
 *
 * @param {string} projectId The id or full path of the project
 * @param {number} mrIID The IID of the merge request (merge_request_iid)
 * @returns
 */
async function getByMR(projectId, mrIID) {
  return api.MergeRequests.pipelines(projectId, mrIID);
}

module.exports = { getByMR };
