const PubSub = require("pubsub-js");

const { logger } = require("../logger");
const projects = require('./projects');
const groups = require('./groups');

PubSub.subscribe("newChallenge", (msg, data) => {
  logger.info("newChallenge Subscription GitLab Handler", data);
  projects.updateProjectInfo(data.newChallengeId);
});
PubSub.subscribe("updateChallenge", (msg, data) => {
  logger.info("updateChallenge Subscription GitLab Handler", data);
  projects.updateProjectInfo(data.challengeId);
});

PubSub.subscribe("newEntrant", groups.createUserFork);