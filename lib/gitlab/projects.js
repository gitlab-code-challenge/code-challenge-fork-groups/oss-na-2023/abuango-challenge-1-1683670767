const db = require("../../db");
const { logger } = require("../logger");
const projects = require("./api/projects");

async function updateProjectInfo(challengeId) {
  try {
    const thisChallenge = await db.challenges.getById(challengeId);
    projects
      .getProject(thisChallenge.project_name)
      .then((val) => {
        db.glprojects.upsert(val);
      })
      .catch((err) =>
        logger.error(
          `Error getting project "${thisChallenge.project_name}"`,
          err
        )
      );
  } catch (error) {
    logger.error("Error in updateProjectInfo", error);
  }
}

module.exports = { updateProjectInfo }