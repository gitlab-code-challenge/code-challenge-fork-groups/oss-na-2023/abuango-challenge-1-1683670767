const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint } = format;
const level = process.env.NODE_ENV !== "production" ? "debug" : "info";

const debugTransport = require('./debug');

const alignedWithColorsAndTime = format.combine(
  format.colorize(),
  format.timestamp(),
  format.align(),
  format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);

const logger = createLogger({
  level,
  format: alignedWithColorsAndTime,
  defaultMeta: { service: "codechallenge-dot-dev" },
  transports: [
    new transports.File({ filename: "error.log", level: "error" }),
    new transports.File({ filename: "combined.log" }),
    new debugTransport({ format: format.simple() }),
  ],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
// if (process.env.NODE_ENV !== "production") {
//   logger.add(
//     new transports.Console({
//       format: format.simple(),
//     })
//   );
// }

module.exports = { logger };
