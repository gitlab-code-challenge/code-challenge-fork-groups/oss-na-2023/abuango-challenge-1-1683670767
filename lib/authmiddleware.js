const { auth } = require("express-openid-connect");

const db = require("../db");
const { logger } = require("../lib/logger");
const pkgjson = require("../package.json");

module.exports = function (app) {
  app.use(
    auth({
      authRequired: false,
      authorizationParams: {
        response_type: "code",
        scope: "openid profile email",
      },
      routes: {
        login: false,
      },
    })
  );

  app.get("/login", (req, res) => res.oidc.login({ returnTo: "/app" }));

  app.get("*", async (req, res, next) => {
    res.locals.version = pkgjson.version.toString().trim();
    res.locals.BASE_URL = process.env.BASE_URL;
    res.locals.ORIGINAL_URL = req.originalUrl;
    if (req.oidc.isAuthenticated()) {
      let { token_type, access_token, isExpired, refresh } =
        req.oidc.accessToken;
      if (isExpired()) {
        logger.info("Refreshing token in middleware");
        try {
          ({ access_token } = await refresh());
        } catch (error) {
          logger.error(error);
          res.oidc.logout({ returnTo: "/" });
        }
      }
      try {
        const openIDuser = await req.oidc.fetchUserInfo();
        const thisUser = await db.users.getOrCreateUser(openIDuser);
        res.locals.currentUser = thisUser;
        req.currentUserId = thisUser.id;
      } catch (error) {
        console.error(error);
      }
    }
    next();
  });
};
