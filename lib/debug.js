const dInfo = require("debug")("codingchallenge:app");
const debug = require("debug")("codingchallenge:debug");
const error = require("debug")("codingchallenge:error");
const Transport = require("winston-transport");
const util = require("util");

module.exports = class YourCustomTransport extends Transport {
  constructor(opts) {
    super(opts);
  }

  log(info, callback) {
    var sym = Object.getOwnPropertySymbols(info).find(function (s) {
      return String(s) === "Symbol(level)";
    });
    const type = info[sym];
    switch (type) {
      case "info":
        dInfo(info.message);
        break;
      case "error":
        error(info.message);
        break;
      case "debug":
        debug(info.message);
        break;
      default:
        dInfo(info.message);
        break;
    }
    setImmediate(() => {
      this.emit("logged", info);
    });

    // Perform the writing to the remote service
    callback();
  }
};
