const textSection = (text) => {
  return {
    type: "section",
    text: {
      type: "mrkdwn",
      text,
    },
  };
};

const textWithLink = (text, url) => {
  return {
    type: "section",
    text: {
      type: "mrkdwn",
      text,
    },
    accessory: {
      type: "button",
      text: {
        type: "plain_text",
        text: "View",
        emoji: true,
      },
      url,
      action_id: "button-action",
      value: "click_me_123",
    },
  };
};

const fieldsSection = (textArray, accessory) => {
  let fields = [];
  for (const txt of textArray) {
    fields.push({
      type: "mrkdwn",
      text: txt,
    });
  }

  const ret = {
    type: "section",
    fields,
  };

  if (accessory) {
    ret.accessory = accessory;
  }
  return ret;
};

const imageAcccesory = (imageUrl, name) => {
  if (imageUrl) {
    return {
      type: "image",
      image_url: imageUrl,
      alt_text: `${name} profile picture`,
    };
  }
  return null;
};

module.exports = {
  textSection,
  fieldsSection,
  imageAcccesory,
  textWithLink,
};
