const dayjs = require("dayjs");
const localizedFormat = require("dayjs/plugin/localizedFormat");
const relativeTime = require("dayjs/plugin/relativeTime");
const updateLocale = require("dayjs/plugin/updateLocale");

//const marked = require("marked");

dayjs.extend(localizedFormat);
dayjs.extend(relativeTime);
dayjs.extend(updateLocale);

dayjs.updateLocale("en", {
  relativeTime: {
    future: "in %s",
    past: "%s ago",
    s: "just seconds",
    m: "a minute",
    mm: "%d minutes",
    h: "an hour",
    hh: "%d hours",
    d: "a day",
    dd: "%d days",
    M: "a month",
    MM: "%d months",
    y: "a year",
    yy: "%d years",
  },
});
const crypto = require("crypto");


/*
function renderMarkdown(mdText){
  return marked.parse(mdText);

}
*/

function section(name, options) {
  if (!this._sections) this._sections = {};
  this._sections[name] = options.fn(this);
  return null;
}

function isEqual(arg1, arg2) {
  return arg1 == arg2;
}

function select(selected, options) {
  return options
    .fn(this)
    .replace(new RegExp(' value="' + selected + '"'), '$& selected="selected"');
}

function formatnumber(value) {
  return parseInt(value).toLocaleString();
}

function now() {
  return dayjs().format("LTS");
}

function randomEmoji() {
  const emojis = [
    "🎉",
    "🎊",
    "🥳",
    "⭐",
    "🙌",
    "😊",
    "🧡",
    "🦊",
    "🏎️",
    "🏁",
    "📣",
    "✅",
  ];

  return emojis[Math.floor(Math.random() * emojis.length)];
}

function timeOrDate(date) {
  const theD = dayjs(date);
  const isToday = dayjs().isSame(theD, "day");

  if (isToday) return theD.format("LT");
  return theD.format("YYYY-MM-DD");
}

function fromNow(date) {
  return dayjs(date).fromNow();
}

function localDate(date) {
  const d = dayjs(date);
  return d.format("LL");
}

function genericAvatar(path_with_namespace) {
  const avatars = [
    "happy-outline",
    "headset-outline",
    "heart-outline",
    "home-outline",
    "pizza-outline",
    "sparkles-outline",
    "telescope-outline",
    "partly-sunny-outline",
  ];

  try {
    const sha = crypto
      .createHash("sha1")
      .update(path_with_namespace)
      .digest("hex");
    const first = sha.substring(0, 1);
    return avatars[parseInt(first, 16) % avatars.length];
  } catch (error) {
    console.error(error);
    return "help-outline";
  }
}

module.exports = {
  section,
  isEqual,
  select,
  formatnumber,
  now,
  randomEmoji,
  timeOrDate,
  fromNow,
  localDate,
  genericAvatar,
//  renderMarkdown,
};
