require("dotenv").config();
const pkgjson = require("./package.json");

var createError = require("http-errors");
var express = require("express");
var hbs = require("hbs");
const sassMiddleware = require("node-sass-middleware");
var path = require("path");
var cookieParser = require("cookie-parser");
var helpers = require('handlebars-helpers');
const comparison = helpers.comparison();

const { logger } = require("./lib/logger");
logger.info(`Current Version: ${pkgjson.version}`);

/**
 * Various libraries required for async and out-of-band tasks
 */
const slack = require("./lib/slack");
const gitlab = require("./lib/gitlab");
const cron = require("./lib/cron");
const sync = require("./db/sync");

var app = express();

const auth = require("./lib/authmiddleware")(app);

// view engine setup
app.set("views", path.join(__dirname, "views"));
hbs.registerPartials(__dirname + "/views/partials", function (err) {});
const hbshelpers = require("./lib/hbshelpers");
hbs.registerHelper("section", hbshelpers.section);
hbs.registerHelper("isEqual", hbshelpers.isEqual);
hbs.registerHelper("select", hbshelpers.select);
hbs.registerHelper("formatnumber", hbshelpers.formatnumber);
hbs.registerHelper("now", hbshelpers.now);
hbs.registerHelper("randomEmoji", hbshelpers.randomEmoji);
hbs.registerHelper("timeOrDate", hbshelpers.timeOrDate);
hbs.registerHelper("fromNow", hbshelpers.fromNow);
hbs.registerHelper("genericAvatar", hbshelpers.genericAvatar);
hbs.registerHelper("localDate", hbshelpers.localDate);
hbs.registerHelper("renderMarkdown", hbshelpers.marked);
hbs.registerHelper("or", comparison.or);
hbs.registerHelper("eq", comparison.eq);
app.set("view engine", "hbs");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(
  sassMiddleware({
    /* Options */
    src: path.join(__dirname, "sass"),
    dest: path.join(__dirname, "public/css"),
    debug: false,
    outputStyle: "compressed",
    prefix: "/css",
  })
);
app.use(express.static(path.join(__dirname, "public")));

/**
 * Routes
 */
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var appRouter = require("./routes/app");
var apiRouter = require("./routes/api");
var filesRouter = require("./routes/files");

app.use("/", indexRouter);
app.use("/app", appRouter);
app.use("/u", usersRouter);
app.use("/api/v1", apiRouter);
app.use("/files", filesRouter);

/**
 * Error handling
 */
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  // TODO: Error tracking / tracing
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
