planter $DATABASE_URL?sslmode=disable \
    -x atdatabases_migrations_applied \
    -x atdatabases_migrations_version \
    -o database.md

sed -i '' 's/@startuml/```plantuml/' database.md 
sed -i '' 's/@enduml/```/' database.md