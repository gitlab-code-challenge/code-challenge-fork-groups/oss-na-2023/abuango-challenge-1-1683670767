const createConnectionPool = require("@databases/pg");
const { logger } = require("../lib/logger");
const debugPostgres = require("debug")("postgres");
const { setPassword } = require("./gcloud");

if (process.env.INSTANCE_CONNECTION_NAME) {
  logger.info("Running in Google Cloud");
  setPassword();
}

const dbConnection = createConnectionPool({
  bigIntMode: "bigint",
  onQueryStart: (_query, { text, values }) => {
    debugPostgres(
      `${new Date().toISOString()} START QUERY ${text.replace(
        /(\r\n|\n|\r)/gm,
        " "
      )} - ${JSON.stringify(values)}`
    );
  },
  onQueryResults: (_query, { text }, results) => {
    debugPostgres(
      `${new Date().toISOString()} END QUERY   ${text.replace(
        /(\r\n|\n|\r)/gm,
        " "
      )} - ${results.length} results`
    );
  },
  onQueryError: (_query, { text }, err) => {
    debugPostgres(
      `${new Date().toISOString()} ERROR QUERY ${text.replace(
        /(\r\n|\n|\r)/gm,
        " "
      )} - ${err.message}`
    );
  },
});

process.once("SIGTERM", () => {
  dbConnection.dispose().catch((ex) => {
    console.error(ex);
  });
});


module.exports = dbConnection;
