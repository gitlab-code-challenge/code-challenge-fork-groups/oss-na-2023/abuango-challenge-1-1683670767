const { sql } = require("@databases/pg");
const PubSub = require("pubsub-js");

const db = require("./connection");

const isStandalone = process.env.STANDALONE;
const standaloneData = require("../data/challenges.json");
const { logger } = require("../lib/logger");
const { iso8601, formatAllDates } = require("../lib/time");

/**
 *
 * @param {number} challengeId The ID for this challenge
 * @returns {import("../db/model").challenge_webhooks.challengeWebhook}
 */
async function getById(challengeId) {
  let challengeWebhook = {};
  if (isStandalone) {
    //TODO: handle standalone
  } else {
    challengeWebhook = await db.query(sql`
            SELECT * from challenge_webhooks
            WHERE challenge_id = ${challengeId}
        `);
  }

  return challengeWebhook[0];
}

async function upsert(hook) {
  const { challenge_id, hook_url, newentrant, levelComplete, channel, icon_emoji, username } = hook;
  const out = await db.query(sql`
      INSERT INTO challenge_webhooks (challenge_id, hook_url, newentrant, levelComplete, channel, icon_emoji, username)
      VALUES(${challenge_id}, ${hook_url}, ${newentrant}, ${levelComplete}, ${channel}, ${icon_emoji}, ${username}) 
      ON CONFLICT (challenge_id) 
      DO 
        UPDATE SET challenge_id = ${challenge_id},
        hook_url = ${hook_url}, 
        newentrant = ${newentrant}, 
        levelComplete = ${levelComplete},
        channel = ${channel},
        icon_emoji = ${icon_emoji},
        username = ${username}
    `);

  return out;
}

module.exports = {
  getById,
  upsert,
};
