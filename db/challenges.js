const { sql } = require("@databases/pg");
const PubSub = require("pubsub-js");

const db = require("./connection");

const isStandalone = process.env.STANDALONE;
const standaloneData = require("../data/challenges.json");
const { logger } = require("../lib/logger");
const { iso8601, formatAllDates } = require("../lib/time");

const request = require('request');

async function getAll(active = true) {
  let challenges = [];
  if (isStandalone) {
    challenges = standaloneData;
  } else {
    challenges = await db.query(sql`
        SELECT *
        FROM challenges
        WHERE active = ${active}
    `);
  }

  challenges = formatAllDates(challenges);

  return challenges;
}

/**
 *
 * @param {int} userId The idea of the user
 * @returns {Object[]}
 */
async function getDetails(userId) {
  let challenges = [];
  if (isStandalone) {
    challenges = standaloneData;
  } else {
    challenges = await db.query(sql`
        SELECT c.*, q.role,
        g.id as glid, g.name as glname, g.description as gldesc,
        g.name_with_namespace, g.web_url, g.avatar_url as proj_avatar_url,
        g.path_with_namespace, g.forks_count, g.star_count, g.group_id, c.enable_fork_group,
        c.fork_group_id, c.fork_group_expiry
        FROM challenges c
        LEFT JOIN (SELECT u.* from users_challenges u WHERE u.user_id = ${userId}) q
        ON q.challenge_id = c.id
        LEFT JOIN glprojects g
        ON c.project_name = g.path_with_namespace
    `);
  }

  challenges = formatAllDates(challenges);

  return challenges;
}

/**
 *
 * @param {number} id The id of the challenge
 * @returns {import("../db/model").challenges.Challenge}
 */
async function getById(id) {
  /** @type {import("../db/model").challenges.Challenge} */
  let challenge = {};
  if (isStandalone) {
    challenge = standaloneData.find((x) => x.id === parseInt(id));
  } else {
    challenge = await db.query(sql`
      SELECT challenges.*, 
      glprojects.id as glid, glprojects.name as glname, glprojects.description as gldesc,
      glprojects.name_with_namespace, glprojects.web_url, glprojects.avatar_url as proj_avatar_url,
      glprojects.path_with_namespace, glprojects.forks_count, glprojects.star_count
      FROM challenges
      LEFT JOIN glprojects
      ON challenges.project_name = glprojects.path_with_namespace
      WHERE challenges.id = ${id}
    `);
    challenge = challenge[0];
  }

  challenge.start_date = iso8601(challenge.start_date);
  challenge.end_date = iso8601(challenge.end_date);

  return challenge;
}



/**
 * Get the latest featured challenge
 *
 * @returns {import("../db/model").challenges.Challenge[]}
 */
async function getFeaturedChallenge() {
  /** @type {import("../db/model").challenges.Challenge} */
  let challenge = {};
  if (isStandalone) {
    challenge = standaloneData.find((x) => x.id === parseInt(id));
  } else {
    challenge = await db.query(sql`
      SELECT challenges.*, 
      glprojects.id as glid, glprojects.name as glname, glprojects.description as gldesc,
      glprojects.name_with_namespace, glprojects.web_url, glprojects.avatar_url as proj_avatar_url,
      glprojects.path_with_namespace, glprojects.forks_count, glprojects.star_count
      FROM challenges
      LEFT JOIN glprojects
      ON challenges.project_name = glprojects.path_with_namespace
      WHERE challenges.featured = TRUE
      ORDER BY start_date
    `);
    challenge = challenge[0];
  }

  if (challenge) {
    challenge.start_date = iso8601(challenge.start_date);
    challenge.end_date = iso8601(challenge.end_date);
  }

  return challenge;
}

/**
 *
 * @param {import("./model").challenges.Challenge} challenge The details of the challenge
 * @param {int} userId
 * @returns
 */
async function create(challenge, userId) {
  const isFeatured = challenge.featured ? true : false;
  const res = await db.query(sql`
    INSERT INTO challenges (name, shortname, description, start_date, end_date, project_name, project_id, created_by, featured, rules) 
    VALUES(${challenge.name}, ${challenge.shortname}, ${challenge.description}, ${challenge.start_date}, ${challenge.end_date}, ${challenge.project_name}, ${challenge.project_id}, ${userId}, ${isFeatured}, ${challenge.rules})
    RETURNING id;
  `);
  const newChallengeId = res[0].id;
  const res2 = await db.query(sql`
    INSERT INTO users_challenges (user_id, challenge_id, role)
    VALUES(${userId}, ${newChallengeId}, 'owner')
  `);
  PubSub.publish("newChallenge", {
    newChallengeId,
    userId,
    source: "db.challenges.create",
  });
  return res;
}

async function isUserSuperAdmin(userId){
  const res = await db.query(sql`
    SELECT super_admin from users
    WHERE id = ${userId}
  `);

  if(res[0]){
    return res[0].super_admin;
  }else{
    return false;
  }
  
}

async function getlbQRContent(challenge_id){
  const res = await db.query(sql`
    SELECT lb_qrcode_content from challenges
    WHERE id = ${challenge_id}
  `);

  if(res[0]){
    return res[0].lb_qrcode_content;
  }else{
    return "";
  }
}

/**
 *
 * @param {import("./model").challenges.Challenge} challenge The details of the challenge
 * @param {int} userId
 * @returns
 */
async function update(challenge, userId) {
  const userProps = await userChallenge(challenge.id, userId);
  const isSuperAdmin = await isUserSuperAdmin(userId);
  if (userProps.isAdmin || isSuperAdmin) {
    const isFeatured = challenge.featured ? true : false;
    const res = await db.query(sql`
      UPDATE challenges SET 
        name = ${challenge.name}, shortname = ${challenge.shortname}, 
        description =  ${challenge.description}, start_date = ${challenge.start_date}, 
        end_date = ${challenge.end_date}, project_name = ${challenge.project_name}, 
        project_id = ${challenge.project_id}, featured = ${isFeatured},
        rules = ${challenge.rules},
        fork_group_id = ${challenge.fork_group_id}, enable_fork_group = ${challenge.enable_fork_group},
        fork_group_expiry = ${challenge.fork_group_expiry}, lb_message_title = ${challenge.lb_message_title}, 
        lb_message_content = ${challenge.lb_message_content}, lb_qrcode_content = ${challenge.lb_qrcode_content}
      WHERE id = ${challenge.id}
      RETURNING id;
    `);
    PubSub.publish("updateChallenge", {
      challengeId: challenge.id,
      userId,
      source: "db.challenges.update",
    });
    return res;
  } else {
    logger.error(
      `Error updating challenge ${challenge.id} because user ${userId} is not admin`
    );
    return null;
  }
}

/**
 * 
 * @param {number} challengeId The ID for this challenge
 * @param {number} userId The user.id for the user to add
 * @param {string} userFNLI The first name and last inital from the user
 * @param {boolean} agree Did the user agree to terms & conditions at the time they signed up
 * @returns 
 */
async function addUserToChallenge(challengeId, userId, userFNLI, agree) {

  const res = await db.query(sql`
    INSERT INTO users_challenges (user_id, challenge_id, role) 
    VALUES(${userId}, ${challengeId}, 'entrant')
  `);
  PubSub.publish("newEntrant", {
    challengeId,
    userId,
    userFNLI,
    agree,
    source: "db.challenges.addUserToChallenge",
  });
  return res;
}

async function getUserProjectFork(challengeId, userId){
  const res = await db.query(sql`
    SELECT project_fork from users_challenges
    WHERE user_id = ${userId}
    AND challenge_id = ${challengeId}
  `);

  if(res[0]){
    return res[0].project_fork;
  }else{
    return false;
  }
  
}

async function userChallenge(challengeId, userId) {
  let user = { isIn: false, isAdmin: false, isOwner: false };
  /** @type {UsersChallenges[]} */
  const res = await db.query(sql`
    SELECT role from users_challenges
    WHERE user_id = ${userId}
    AND challenge_id = ${challengeId}
  `);

  if (res[0]) {
    const role = res[0].role;
    user.isIn = true;
    switch (role) {
      case "owner":
        user.isOwner = true;
        user.isAdmin = true;
        break;
      case "admin":
        user.isAdmin = true;
        break;
      default:
        user.isAdmin = false;
    }
  }

  return user;
}

async function changeActive(challengeId, active = false) {
  return db.query(sql`
    UPDATE challenges
    SET active = ${active}
    WHERE id = ${challengeId}
  `);
}

async function getByShortName(shortname) {
  let challenge = {};
  if (isStandalone) {
    challenge = standaloneData.find((x) => x.shortname === parseInt(shortname));
  } else {
    challenge = await db.query(sql`
      SELECT challenges.*
      FROM challenges
      WHERE challenges.shortname = ${shortname}
    `);
    challenge = challenge[0];
  }

  if (challenge) {
    challenge.start_date = iso8601(challenge.start_date);
    challenge.end_date = iso8601(challenge.end_date);
  }

  return challenge;
}

module.exports = {
  getAll,
  getDetails,
  getById,
  create,
  userChallenge,
  addUserToChallenge,
  update,
  changeActive,
  getByShortName,
  getFeaturedChallenge,
  getUserProjectFork,
  getlbQRContent
};
