const { sql } = require("@databases/pg");
const PubSub = require("pubsub-js");

const db = require("./connection");

const isStandalone = process.env.STANDALONE;
const standaloneData = require("../data/challenges.json");
const { logger } = require("../lib/logger");
const { iso8601, formatAllDates } = require("../lib/time");

async function getIncomplete(challengeId, levelId) {
  let incomplete = [];
  if (isStandalone) {
    //TODO: standalone data handling
  } else {
    incomplete = await db.query(sql`
            SELECT l.challenge_id, l.level, l.title, l.completed,
            l.type, l.details, l.after, l.contains, 
            g.project_name,
            u.user_id, u.role, p.name, p.picture, p.gitlab_handle,
            c.started, c.start_time, c.completed, c.complete_time
            FROM levels l
            LEFT OUTER JOIN users_challenges u
            ON l.challenge_id = u.challenge_id
            LEFT JOIN users p
            ON u.user_id = p.id
            LEFT JOIN challenges g
            ON l.challenge_id = g.id
            LEFT OUTER JOIN levels_completed c
            ON l.challenge_id = c.challenge_id AND u.user_id = c.user_id
            WHERE l.id = ${levelId} AND l.challenge_id = ${challengeId} AND c.completed IS NULL;
        `);
  }

  return incomplete;
}

/**
 *
 * @param {int} levelId The id for the level (level_id)
 * @param {int} challengeId The challenge id (challenge_id)
 * @param {int} userId The user that completed the challenge (user_id)
 * @param {string} ref Reference to the completion (what MR, issue, etc. made this completed?)
 * @param {boolean} [automated=true] Was this an automated addition?
 * @returns
 */
async function addCompletion(
  levelId,
  challengeId,
  userId,
  ref,
  automated = true
) {
  const res = await db.query(sql`
    INSERT INTO levels_completed (level_id, challenge_id, user_id, ref, completed, complete_time, automated)
    VALUES (${levelId}, ${challengeId}, ${userId}, ${ref}, TRUE, NOW(), ${automated})
  `);
  PubSub.publish("levelCompleted", { levelId, challengeId, userId, ref });
  return res;
}

/**
 *
 * @param {int} levelId The id for this level (level_id)
 * @returns {Promise<LevelsCompleted[]>} All of the users who have completed the level
 */
async function getComplete(levelId) {
  const res = await db.query(sql`
  SELECT * from levels_completed
  WHERE level_id = ${levelId}
 `);
  return res;
}

/**
 * @typedef {Object} LevelStats
 * @property {number} level_id The ID for the level
 * @property {number} completed The number of entrants who have completed that level
 */

/**
 *
 * @param {int} challengeId The ID of the challenge to return
 * @returns {Promise<LevelStats[]>}
 */
async function getLevelStats(challengeId) {
  const res = await db.query(sql`
    SELECT level_id, count(*) as completed
    FROM levels_completed
    WHERE challenge_id = ${challengeId}
    GROUP BY level_id
  `);
  return res;
}

/**
 * 
 * @param {number} challengeId The ID for the challenge (challenge.id)
 * @param {number=} limit Limit the query to the last few completed (basesd on levels_completed.complete_time)
 * @returns 
 */
async function getCompleteByChallenge(challengeId, limit) {
  let thisQ = sql`
    SELECT c.*, l.level 
    FROM levels_completed c
    LEFT JOIN levels l ON c.level_id = l.id
    WHERE c.challenge_id = ${challengeId}
  `

  if (limit) {
    thisQ = sql`
      ${thisQ} 
      ORDER BY complete_time DESC
      LIMIT ${limit}
    `
  }

  const res = await db.query(thisQ);
  return res;
}

/**
 *
 * @param {int} levelId The id (level_id) for this level
 * @param {int} completed The number of entrants who have completed the level
 * @returns
 */
async function updateLevelCompleted(levelId, completed) {
  const res = await db.query(sql`
    UPDATE levels 
    SET completed = ${completed}
    WHERE id = ${levelId}
  `);
  return res;
}

async function deleteLevelCompleted(levelId, challengeId, userId) {
  const res = await db.query(sql`
    DELETE FROM levels_completed
    WHERE level_id = ${levelId}
    AND challenge_id = ${challengeId}
    AND user_id = ${userId}
  `);
  PubSub.publish("levelCompletedDeleted", { levelId, challengeId, userId });
  return res;
}

async function singleRecord(levelId, challengeId, userId) {
  const res = await db.query(sql`
    SELECT *
    FROM levels_completed
    WHERE level_id = ${levelId}
    AND challenge_id = ${challengeId}
    AND user_id = ${userId}
  `);
  return res[0];
}

/**
 *
 * @param {number} challengeId The challenge id
 * @param {number} userId The user id
 * @returns {Promise<LevelsCompleted[]>}
 */
async function byUser(challengeId, userId) {
  const res = await db.query(sql`
    SELECT *
    FROM levels_completed
    WHERE challenge_id = ${challengeId}
    AND user_id = ${userId}
  `);
  return res;
}

module.exports = {
  getIncomplete,
  getComplete,
  getCompleteByChallenge,
  addCompletion,
  getLevelStats,
  updateLevelCompleted,
  deleteLevelCompleted,
  singleRecord,
  byUser,
};
