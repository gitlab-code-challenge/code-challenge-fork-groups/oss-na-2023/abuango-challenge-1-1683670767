const { sql } = require("@databases/pg");
const db = require("./connection");

/**
 *
 * @param {number} id The gitlab_id for this user
 * @returns {import("../db/model").users.User}
 */
async function getByGitLabID(id) {
  return db.query(sql`
        SELECT *
        FROM users
        WHERE gitlab_id = ${id}
    `);
}

/**
 *
 * @param {number} username The gitlab_handle for this user
 * @returns {import("../db/model").users.User}
 */
async function getByGitLabHandle(username) {
  const res = await db.query(sql`
        SELECT *
        FROM users
        WHERE gitlab_handle = ${username}
    `);
  return res[0];
}

/**
 *
 * @param {number} id The ID for this user
 * @returns {import("../db/model").users.User}
 */
async function getById(id) {
  const user = await db.query(sql`
        SELECT *
        FROM users
        WHERE id = ${id}
    `);
  return user[0];
}

async function getOrCreateUser(openIduser) {
  let user = {};
  let exists = await getByGitLabID(openIduser.sub);

  if (exists.length == 0) {
    await db.query(sql`
        INSERT INTO users (name, gitlab_id, gitlab_handle, email, picture, last_login) 
        VALUES(${openIduser.name}, ${openIduser.sub}, ${openIduser.nickname}, ${openIduser.email}, ${openIduser.picture}, NOW())
      `);
    const newUser = await getByGitLabID(openIduser.sub);
    user = await newUser[0];
  } else {
    user = exists[0];
  }

  return user;
}

module.exports = {
  getByGitLabID,
  getByGitLabHandle,
  getOrCreateUser,
  getById,
};
