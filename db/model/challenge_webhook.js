/**
 * @typedef {Object} challengeWebhook
 * @property {number} challenge_id The id for the related challenge (challenge.id)
 * @property {String} hook_url The slack URL for the integration
 * @property {boolean} newentrant Whether or not to notify on new entrants
 * @property {boolean} levelcomplete Whether or not to notify on level completion
 * @property {Date} created_at Date/time the record was created
 * @property {Date} updated_at Date/time the record was updated
 * @property {String} channel The Slack channel ID to post to
 * @property {String} icon_emoji The slack emoji to use as the icon for the webhook
 * @property {String} username The user name to use when posting to Slack
 */

module.exports = { challengeWebhook };
