/**
 * @typedef {Object} User
 * @property {number} id User ID
 * @property {String} name User name
 * @property {String} gitlab_handle The gitlab handle for the user
 * @property {number} gitlab_id The gitlab id for the user
 * @property {String} email The email address for the user
 * @property {String} picture The URL for the user's picture
 * @property {Date} last_login The date/time the user last logged in
 * @property {Date} created_at Date/time the record was created
 * @property {Date} updated_at Date/time the record was updated
 */

module.exports = { User };
