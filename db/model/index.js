const challenges = require("./challenges");
const levels = require("./levels");
const users_challenges = require("./users_challenges");
const challenge_webhooks = require("./challenge_webhook");
const levels_completed = require("./levels_completed");
const users = require("./users");
const user_badges = require("./user_badges");
const signups = require("./signup");

module.exports = {
  challenges,
  users,
  levels,
  users_challenges,
  levels_completed,
  challenge_webhooks,
  user_badges,
  signups,
};
