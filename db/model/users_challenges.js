/**
 * @typedef {Object} UsersChallenges
 * @property {number} id The ID for the entry
 * @property {number} user_id The user id for the entrant (users.id)
 * @property {number} challenge_id The id for the related challenge (challenge.id)
 * @property {(owner|admin|entrant)} role The role the user has in this challenge
 * @property {Date} created_at Date/time the record was created
 * @property {Date} updated_at Date/time the record was updated
 */

/**
 * @typedef {Object} EntrantDetails
 * @property {string} name The name from users.name
 * @property {string} gitlab_handle The user's GitLab username from users.gitlab_handle
 * @property {int} gitlab_id The GitLab ID from users.gitlab_id
 * @property {string} email The user's email from users.email
 * @property {string} picture The url to the user's profile picture from users.picture
 */

/**
 * @typedef {UsersChallenges & EntrantDetails} Entrant
 */

module.exports = {
  UsersChallenges,
  EntrantDetails,
  Entrant,
};
