/**
 * @typedef {Object} Level
 * @property {number} id The ID for the level
 * @property {number} challenge_id The id for the related challenge (challenge.id)
 * @property {string} title Name of this level
 * @property {string} description Describe this level
 * @property {("MR"|"ISSUE"|"COMMIT"|"PIPELINE")} type They type of completion for this level (merge request)
 * @property {Object} details Any extra JSON details for this level
 * @property {string} after A date that the MR, issue, etc must be after to count (in the format 2022-01-31)
 * @property {string} contains A string that the MR, issue, etc must contain to count as completed
 * @property {number} created_by The user ID of the person who created this level
 * @property {number} completed The nubmer of entrants who have completed this level
 * @property {Date} created_at Date/time the record was created
 * @property {Date} updated_at Date/time the record was updated
 */

module.exports = { Level };
