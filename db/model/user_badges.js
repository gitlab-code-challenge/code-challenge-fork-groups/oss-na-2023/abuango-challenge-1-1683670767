/**
 * @typedef {Object} UserBadges
 * @property {number} id The ID for this user badge
 * @property {number} level_id The ID for the level
 * @property {number} challenge_id The id for the related challenge (challenge.id)
 * @property {number} user_id The id for the user who completed this challenge (users.id)
 * @property {string} badge The URL top the badge image
 * @property {string} ref Optional URL that references the completed product
 * @property {string} challenge_name The name of the challenge when this badge was earned
 * @property {string} level_title The title of the level when this badge was earned
 * @property {Date} created_at Date/time the record was created
 * @property {Date} updated_at Date/time the record was updated
 */

module.exports = { UserBadges };
