/**
 * @typedef {Object} LevelsCompleted
 * @property {number} level_id The ID for the level
 * @property {number} challenge_id The id for the related challenge (challenge.id)
 * @property {number} user_id The id for the user who completed this challenge (users.id)
 * @property {boolean} started If the challenge has been started
 * @property {Date} start_time The time that it was started
 * @property {boolean} completed If the challenge has been completed or not
 * @property {Date} complete_time The time that it was completed
 * @property {Date} created_at Date/time the record was created
 * @property {Date} updated_at Date/time the record was updated
 */

module.exports = { LevelsCompleted };
