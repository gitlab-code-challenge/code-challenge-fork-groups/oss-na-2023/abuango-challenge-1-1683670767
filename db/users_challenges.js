const { sql } = require("@databases/pg");
const PubSub = require("pubsub-js");

const db = require("./connection");

const isStandalone = process.env.STANDALONE;
const standaloneData = require("../data/challenges.json");
const { logger } = require("../lib/logger");
const { iso8601, formatAllDates } = require("../lib/time");

/**
 * @typedef {Object} UserChallengesDetails
 * @property {number} level_id The ID for the level
 * @property {number} completed The number of entrants who have completed that level
 */

/**
 *
 * @param {int} challengeId The id of the challenge (users_challenges.challenge_id)
 * @returns
 */
async function getEntrants(challengeId) {
  let entrants = [];
  if (isStandalone) {
    // TODO: handle standalone data
  } else {
    entrants = await db.query(sql`
            SELECT users_challenges.*,
            users.name, users.gitlab_handle,
            users.gitlab_id, users.email,
            users.picture
            FROM users_challenges
            LEFT JOIN users
            ON users_challenges.user_id = users.id
            WHERE users_challenges.challenge_id = ${challengeId}
        `);
  }
  return entrants;
}

async function deleteEntrant(challengeId, userId) {
  const ret = await db.query(sql`
    DELETE FROM users_challenges
    WHERE challenge_id = ${challengeId}
    AND user_id = ${userId}
    AND role != 'owner'
  `);
  return ret;
}

async function promoteEntrant(challengeId, userId) {
  const ret = await db.query(sql`
    UPDATE users_challenges
    SET role = 'admin'
    WHERE challenge_id = ${challengeId}
    AND user_id = ${userId}
    AND role != 'owner'
  `);
  return ret;
}

module.exports = {
  getEntrants,
  deleteEntrant,
  promoteEntrant,
};
