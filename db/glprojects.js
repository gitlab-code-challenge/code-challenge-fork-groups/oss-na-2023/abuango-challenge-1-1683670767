const { sql } = require("@databases/pg");
const db = require("./connection");
const util = require("util");
const { logger } = require("express-winston");
const { Console } = require("console");

async function getByGitLabID(id) {
  const project = await db.query(sql`
        SELECT *
        FROM glprojects
        WHERE id = ${id}
    `);
    return project;
}

//name_with_namespace
async function getByNamespace(path_with_namespace) {
  const project = await db.query(sql`
        SELECT *
        FROM glprojects
        WHERE path_with_namespace = ${path_with_namespace}
    `);
  return project;
}

async function updateUserProjectFork(path_with_namespace, userId,challengeId){
  console.log(path_with_namespace+" "+userId+" "+challengeId);
  const updateFork = await db.query(sql`UPDATE users_challenges SET project_fork = ${path_with_namespace} WHERE user_id = ${userId} AND challenge_id = ${challengeId}`);

return updateFork;

}

async function upsert(glproject) {
  const { id, name, name_with_namespace, path, path_with_namespace, default_branch, http_url_to_repo, web_url, avatar_url, forks_count, star_count, visibility, description, namespace } = glproject;
  let groupId = 0;
  if (namespace.kind === 'group') groupId = namespace.id;
  const out = await db.query(sql`
    INSERT INTO glprojects (id, name, name_with_namespace, path, path_with_namespace, default_branch, http_url_to_repo, web_url, avatar_url, forks_count, star_count, visibility, description, group_id)
    VALUES(${id}, ${name}, ${name_with_namespace}, ${path}, ${path_with_namespace}, ${default_branch}, ${http_url_to_repo}, ${web_url}, ${avatar_url}, ${forks_count}, ${star_count}, ${visibility}, ${description}, ${groupId}) 
    ON CONFLICT (id) 
    DO 
      UPDATE SET id = ${id}, 
      name = ${name}, 
      name_with_namespace = ${name_with_namespace}, 
      path = ${path}, 
      path_with_namespace = ${path_with_namespace}, 
      default_branch = ${default_branch}, 
      http_url_to_repo = ${http_url_to_repo}, 
      web_url = ${web_url}, 
      avatar_url = ${avatar_url}, 
      forks_count = ${forks_count}, 
      star_count = ${star_count}, 
      visibility = ${visibility}, 
      description = ${description},
      group_id = ${groupId}
  `)

  return out;
}

module.exports = { getByGitLabID, getByNamespace, upsert, updateUserProjectFork };
