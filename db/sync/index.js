const PubSub = require("pubsub-js");

const signups = require('./signups');

PubSub.subscribe("newEntrant", signups.addSignup);
PubSub.subscribe("levelCompleted", signups.markComplete);

