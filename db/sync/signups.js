const { logger } = require("../../lib/logger");
const db = require("../index");

/**
 * Add Sign Up data sync
 * 
 * This function is called on PubSub of newEntrant when a new person enters a contest.  
 * We track what they listed their name as, as well as the fact that they agreed to the terms
 * 
 * @param {Object} msg 
 * @param {Object} data 
 */
function addSignup(msg, data) {
    /** @type {import('../model/index').signups.SignUp} */
    const signup = {
        challenge_id: data.challengeId,
        user_id: data.userId,
        lastname_fi: getInitials(data.userFNLI),
        agree_to_rules: data.agree,
    }
    db.signups.create(signup)
    .catch(e => {
        logger.error("Error adding signup record", e);
        console.error(e)
    })
}

function getInitials(fullName){
    var parts = fullName.split(' ');
  var initials = parts[0]+' ';
  for (var i = 1; i < parts.length; i++) {
    if (parts[i].length > 0 && parts[i] !== '') {
      initials += parts[i][0]+".";
    }
  }
  return initials;
}


function markComplete(msg, data) {
    const { levelId, challengeId, userId, ref } = data;
    db.signups.markCompleted(challengeId, userId)
    .catch(e => {
        logger.error("Error marking signup levels_completed", e);
        console.error(e);
    })
}

module.exports = {
    addSignup,
    markComplete,
    getInitials,
}