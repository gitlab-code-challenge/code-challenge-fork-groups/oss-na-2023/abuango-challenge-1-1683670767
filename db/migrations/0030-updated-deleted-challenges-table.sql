ALTER TABLE deleted_users_challenges
ADD COLUMN project_fork varchar(250);

ALTER TABLE deleted_challenges
ADD COLUMN enable_fork_group BOOLEAN DEFAULT FALSE,
ADD COLUMN fork_group_id INTEGER,
ADD COLUMN fork_group_expiry INTEGER DEFAULT 7;