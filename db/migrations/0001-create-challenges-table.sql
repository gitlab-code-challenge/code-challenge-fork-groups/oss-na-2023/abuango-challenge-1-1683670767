CREATE OR REPLACE FUNCTION trigger_set_timestamp()
  RETURNS TRIGGER AS $$
  BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
  END;
  $$ LANGUAGE plpgsql;

CREATE TABLE challenges (
  id            serial PRIMARY KEY,
  name          varchar(40) NOT NULL,
  description   TEXT,
  start_date    DATE,
  active        BOOL NOT NULL DEFAULT TRUE,
  project_name  TEXT,
  project_id    INTEGER,
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON challenges
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();