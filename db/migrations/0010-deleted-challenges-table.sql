CREATE TABLE "deleted_challenges" AS TABLE "challenges" WITH NO DATA;

CREATE FUNCTION moveDeletedChallenges() RETURNS trigger AS $$
  BEGIN
    INSERT INTO "deleted_challenges" VALUES((OLD).*);
    RETURN OLD;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER moveDeletedChallenges
BEFORE DELETE ON "challenges"
FOR EACH ROW
EXECUTE PROCEDURE moveDeletedChallenges();