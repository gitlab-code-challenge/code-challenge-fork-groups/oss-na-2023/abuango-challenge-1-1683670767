ALTER TABLE challenges
ADD COLUMN enable_fork_group BOOLEAN DEFAULT FALSE,
ADD COLUMN fork_group_id INTEGER;