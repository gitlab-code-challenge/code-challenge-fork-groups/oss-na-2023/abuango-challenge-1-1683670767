CREATE TABLE levels_completed (
  level_id      INTEGER NOT NULL,
  challenge_id  INTEGER NOT NULL,
  user_id       INTEGER NOT NULL,
  started       BOOLEAN,
  start_time    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  completed     BOOLEAN,
  complete_time TIMESTAMPTZ,
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  PRIMARY KEY (level_id, challenge_id, user_id)
);

ALTER TABLE levels_completed
ADD CONSTRAINT fk_challenges
    FOREIGN KEY(challenge_id)
    REFERENCES challenges(id),
ADD CONSTRAINT fk_levels
    FOREIGN KEY(level_id)
    REFERENCES levels(id),
ADD CONSTRAINT fk_users
    FOREIGN KEY(user_id)
    REFERENCES users(id);

CREATE TRIGGER set_timestamp_levels_completed
  BEFORE UPDATE ON levels_completed
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();