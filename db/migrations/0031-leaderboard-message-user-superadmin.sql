ALTER TABLE challenges
ADD COLUMN lb_message_title varchar(250),
ADD COLUMN lb_message_content varchar(250),
ADD COLUMN lb_qrcode_content varchar(250);

ALTER TABLE users
ADD COLUMN super_admin BOOLEAN DEFAULT FALSE;