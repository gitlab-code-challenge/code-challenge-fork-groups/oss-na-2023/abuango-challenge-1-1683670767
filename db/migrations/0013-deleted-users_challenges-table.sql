CREATE TABLE "deleted_users_challenges" AS TABLE "users_challenges" WITH NO DATA;

CREATE FUNCTION moveDeletedUserChallenges() RETURNS trigger AS $$
  BEGIN
    INSERT INTO "deleted_users_challenges" VALUES((OLD).*);
    RETURN OLD;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER moveDeletedUserChallenges
BEFORE DELETE ON "users_challenges"
FOR EACH ROW
EXECUTE PROCEDURE moveDeletedUserChallenges();

ALTER TABLE users_challenges
DROP CONSTRAINT fk_challenges;

ALTER TABLE users_challenges 
ADD CONSTRAINT fk_challenges
    FOREIGN KEY(challenge_id)
    REFERENCES challenges(id)
    ON DELETE CASCADE; 