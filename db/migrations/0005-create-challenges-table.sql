CREATE TABLE users_challenges (
  id            serial PRIMARY KEY,
  user_id       INT NOT NULL,
  challenge_id  INT NOT NULL,
  role          varchar(10) NOT NULL,
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

ALTER TABLE users_challenges
ADD CONSTRAINT fk_users
    FOREIGN KEY(user_id)
    REFERENCES users(id),
ADD CONSTRAINT fk_challenges
    FOREIGN KEY(challenge_id)
    REFERENCES challenges(id); 

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON users_challenges
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();