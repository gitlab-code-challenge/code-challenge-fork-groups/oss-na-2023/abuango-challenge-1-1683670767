const { sql } = require("@databases/pg");

const db = require("./connection");

/**
 *
 * @returns {Object[]}
 */
async function getMigrations() {
  const res = db.query(sql`
        SELECT id, index, name, applied_at, ignored_error, obsolete
        FROM atdatabases_migrations_applied;
    `);
  return res;
}

async function getTime() {
  const res = db.query(sql`
        SELECT NOW();
    `);
  return res;
}

module.exports = {
  getMigrations,
  getTime,
};
