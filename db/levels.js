const { sql } = require("@databases/pg");
const PubSub = require("pubsub-js");

const db = require("./connection");
const { userChallenge } = require("./challenges");

const isStandalone = process.env.STANDALONE;
const standaloneData = require("../data/challenges.json");
const { logger } = require("../lib/logger");
const { iso8601, formatAllDates } = require("../lib/time");

/**
 *
 * @param {number} challengeId The challenge ID for this level.
 * @returns {import("./model").levels.Level[]} The levels for this challenge
 */
async function getAllByChallengeId(challengeId) {
  let levels = [];
  if (isStandalone) {
    // TODO: standalone data
  } else {
    levels = await db.query(sql`
            SELECT * 
            FROM LEVELS
            WHERE challenge_id = ${challengeId}
            ORDER BY level
        `);
  }

  return levels;
}

/**
 *
 * @param {number} levelId The ID of the level
 * @returns {import("./model").levels.Level} The level object
 */
async function getById(levelId) {
  let level = {};
  if (isStandalone) {
    // TODO: standalone data
  } else {
    level = await db.query(sql`
            SELECT * 
            FROM LEVELS 
            WHERE id = ${levelId}
        `);
    level = level[0];
  }

  return level;
}

/**
 *
 * @param {Object} level The details of the level
 * @param {string} level.title The title for this level
 * @param {string} level.description The description of how to achieve this level
 * @param {int} level.level The level number / order of this level
 * @param {string} level.type The type of level - accepted valeus are ISSUE, MR, COMMIT, and PIPELINE
 * @param {string} level.after Any condition that has to occure before the completion of this level (e.g. issue number or commit hash)
 * @param {string} level.contains Any string that must be contained in the created item (MR, issue, etc).
 * @param {int} challengeId The challenge ID for this level.
 * @param {int} userId The userId of the creator of this level
 */
async function create(level, challengeId, userId) {
  const res = await db.query(sql`
        INSERT INTO levels (challenge_id, created_by, title, level, description, type, after, contains)
        VALUES (${challengeId}, ${userId}, ${level.title}, ${level.level}, ${level.description}, ${level.type}, ${level.after}, ${level.contains})
        RETURNING id;
    `);
  const newLevelId = res[0].id;
  PubSub.publish("newLevel", {
    newLevelId,
    userId,
    challengeId,
    source: "db.levels.create",
  });
  return res;
}

/**
 *
 * @param {Object} level The details of the level
 * @param {string} level.title The title for this level
 * @param {string} level.description The description of how to achieve this level
 * @param {int} level.level The level number / order of this level
 * @param {string} level.type The type of level - accepted valeus are ISSUE, MR, COMMIT, and PIPELINE
 * @param {string} level.after Any condition that has to occure before the completion of this level (e.g. issue number or commit hash)
 * @param {string} level.contains Any string that must be contained in the created item (MR, issue, etc).
 * @param {int} challengeId The challenge ID for this level.
 * @param {int} userId The userId making this change
 */
async function update(level, challengeId, userId) {
  const userProps = await userChallenge(challengeId, userId);
  if (userProps.isAdmin) {
    const res = await db.query(sql`
            UPDATE levels SET
              title = ${level.title}, description = ${level.description},
              level = ${level.level}, type = ${level.type},
              after = ${level.after}, contains = ${level.contains},
              badge = ${level.badge}
            WHERE id = ${level.id}
            RETURNING id;
        `);
    PubSub.publish("updateLevel", {
      challengeId,
      levelId: level.id,
      userId,
      source: "db.levels.update",
    });
  } else {
    logger.error(
      `Error updating level ${level.id} because user ${userId} is not an admin`
    );
    return null;
  }
}

async function saveBadge(levelId, url) {
  const res = await db.query(sql`
        UPDATE levels SET
          badge = ${url}
        WHERE id = ${levelId}
        RETURNING id;
    `);
  return res;
}

module.exports = {
  getAllByChallengeId,
  getById,
  create,
  update,
  saveBadge,
};
